/**
 * @file conversions.cpp
 * @author Joe (joe.sedutto@silvertech.com)
 * @brief Various conversion utilities
 * @date 2022-05-10
 *
 * @copyright Copyright (c) 2022
 *
 */

const float millibarInchConversionFactor = 0.02978125309;

/**
 * @brief Converts Millibar to Inches of Mercury
 *
 * @param millibar Value in millibars
 * @return int Value in Inches of Mercury
 */
float milibarToInchMG(float millibar)
{
    return round((float(millibar) * millibarInchConversionFactor) * 100);
}

/**
 * @brief Converts Degrees Centigrade to Degrees Fahrenheit
 *
 * @param celsius Value in C
 * @return int Value in F
 */
float celsiusToFahrenheit(float celsius)
{
    float deg_f;
    deg_f = (celsius * 1.8) + 32.0;
    return deg_f;
}

/**
 * @brief Converts Kilometers per hour to Miles per hour
 *
 * @param kmh Value in Kmh
 * @return float Value in Mph
 */
float kmhToMph(float kmh)
{
    return kmh / 1.609;
}

/**
 * @brief Converts Kilometers per hour to knots
 *
 * @param kmh Value in Kmh
 * @return float Value in kts
 */
float kmhToKnots(float kmh)
{
    return kmh / 1.852;
}
