#ifndef _BOARD_CONFIG_H
#define _BOARD_CONFIG_H

// Compass
const uint8_t WINDA_PIN = 48;
const uint8_t WINDB_PIN = 49;
const uint8_t WINDC_PIN = 46;
const uint8_t WINDD_PIN = 47;

// Segments
const uint8_t SS_A = 25;
const uint8_t SS_B = 24;
const uint8_t SS_C = 23;
const uint8_t SS_D = 22;
const uint8_t S_DP = 42;
const uint8_t S_A = 43;
const uint8_t S_B = 40;
const uint8_t S_C = 41;
const uint8_t S_D = 38;
const uint8_t S_E = 39;
const uint8_t S_F = 36;
const uint8_t S_G = 37;

// Buttons
const uint8_t FRONT_INPUT_SEL = 10;
const uint8_t BACK_INPUT_SEL = 11;

const uint8_t WindChillButtonPin = 27;
const uint8_t MaxTempButtonPin = 26;
const uint8_t BaroMaxButtonPin = 29;
const uint8_t BaroMinButtonPin = 28;
const uint8_t MinTempButtonPin = 31;
const uint8_t ClearButtonPin = 30;
// 12/24 hr
const uint8_t WindAverageButtonPin = 32;
const uint8_t BaroRateButtonPin = 35;
const uint8_t PeakGustButtonPin = 34;

const uint8_t BUTTON_BOUNCE_INTERVAL = 5;

// Special
const uint8_t mphLampPin = 4;

#endif
