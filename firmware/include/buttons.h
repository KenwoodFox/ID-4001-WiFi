/**
 * @file buttons.cpp
 * @author Joe (joe.sedutto@silvertech.com)
 * @brief Methods, constructors and data types for all the various buttons and switches.
 * @date 2022-05-10
 *
 * @copyright Copyright (c) 2022
 *
 */

// External libs
#include "Bounce2.h"

#include "boardConfig.h"

// Construct Debouncers
// Front
Bounce2::Button clearBtn = Bounce2::Button();
Bounce2::Button minTempBtn = Bounce2::Button();
Bounce2::Button maxTempBtn = Bounce2::Button();
Bounce2::Button windChillBtn = Bounce2::Button();
Bounce2::Button minBaroBtn = Bounce2::Button();
Bounce2::Button maxBaroBtn = Bounce2::Button();
Bounce2::Button baroRateBtn = Bounce2::Button();
Bounce2::Button peakGustBtn = Bounce2::Button();
Bounce2::Button windAvgBtn = Bounce2::Button();

// Back
Bounce2::Button windSpeedKnotSw = Bounce2::Button();
Bounce2::Button windSpeedKmhSw = Bounce2::Button();
Bounce2::Button baroUnitsSw = Bounce2::Button();
Bounce2::Button tempUnitsSw = Bounce2::Button();
Bounce2::Button timeStartSw = Bounce2::Button();
Bounce2::Button tempAutoHoldSw = Bounce2::Button();
Bounce2::Button hrMthSw = Bounce2::Button();
Bounce2::Button minDaySw = Bounce2::Button();
Bounce2::Button timeAutoHoldSw = Bounce2::Button();
Bounce2::Button railwayTimeSw = Bounce2::Button();

/**
 * @brief Configure the ID-4001's buttons
 *
 * @author Joe (joe.sedutto@silvertech.com)
 */
void configureButtons()
{
    // Pin Setup
    pinMode(FRONT_INPUT_SEL, INPUT_PULLUP);
    pinMode(BACK_INPUT_SEL, INPUT_PULLUP);

    // Attach all buttons, they are done in pairs because front and back share pins
    clearBtn.attach(FRONT_INPUT_SEL, INPUT_PULLUP);
    windSpeedKnotSw.attach(BACK_INPUT_SEL, INPUT_PULLUP);

    minTempBtn.attach(FRONT_INPUT_SEL, INPUT_PULLUP);
    windSpeedKmhSw.attach(BACK_INPUT_SEL, INPUT_PULLUP);

    maxTempBtn.attach(FRONT_INPUT_SEL, INPUT_PULLUP);
    baroUnitsSw.attach(BACK_INPUT_SEL, INPUT_PULLUP);

    windChillBtn.attach(FRONT_INPUT_SEL, INPUT_PULLUP);
    tempUnitsSw.attach(BACK_INPUT_SEL, INPUT_PULLUP);

    minBaroBtn.attach(FRONT_INPUT_SEL, INPUT_PULLUP);
    timeStartSw.attach(BACK_INPUT_SEL, INPUT_PULLUP);

    maxBaroBtn.attach(FRONT_INPUT_SEL, INPUT_PULLUP);
    tempAutoHoldSw.attach(BACK_INPUT_SEL, INPUT_PULLUP);

    baroRateBtn.attach(FRONT_INPUT_SEL, INPUT_PULLUP);
    hrMthSw.attach(BACK_INPUT_SEL, INPUT_PULLUP);

    peakGustBtn.attach(FRONT_INPUT_SEL, INPUT_PULLUP);
    minDaySw.attach(BACK_INPUT_SEL, INPUT_PULLUP);

    windAvgBtn.attach(FRONT_INPUT_SEL, INPUT_PULLUP);
    timeAutoHoldSw.attach(BACK_INPUT_SEL, INPUT_PULLUP);

    railwayTimeSw.attach(BACK_INPUT_SEL, INPUT_PULLUP);

    // Set button intervals
    clearBtn.interval(BUTTON_BOUNCE_INTERVAL);
    windSpeedKnotSw.interval(BUTTON_BOUNCE_INTERVAL);

    minTempBtn.interval(BUTTON_BOUNCE_INTERVAL);
    windSpeedKmhSw.interval(BUTTON_BOUNCE_INTERVAL);

    maxTempBtn.interval(BUTTON_BOUNCE_INTERVAL);
    baroUnitsSw.interval(BUTTON_BOUNCE_INTERVAL);

    windChillBtn.interval(BUTTON_BOUNCE_INTERVAL);
    tempUnitsSw.interval(BUTTON_BOUNCE_INTERVAL);

    minBaroBtn.interval(BUTTON_BOUNCE_INTERVAL);
    timeStartSw.interval(BUTTON_BOUNCE_INTERVAL);

    maxBaroBtn.interval(BUTTON_BOUNCE_INTERVAL);
    tempAutoHoldSw.interval(BUTTON_BOUNCE_INTERVAL);

    baroRateBtn.interval(BUTTON_BOUNCE_INTERVAL);
    hrMthSw.interval(BUTTON_BOUNCE_INTERVAL);

    peakGustBtn.interval(BUTTON_BOUNCE_INTERVAL);
    minDaySw.interval(BUTTON_BOUNCE_INTERVAL);

    windAvgBtn.interval(BUTTON_BOUNCE_INTERVAL);
    timeAutoHoldSw.interval(BUTTON_BOUNCE_INTERVAL);

    railwayTimeSw.interval(BUTTON_BOUNCE_INTERVAL);

    // Set Pressed States
    clearBtn.setPressedState(LOW);
    windSpeedKnotSw.setPressedState(LOW);

    minTempBtn.setPressedState(LOW);
    windSpeedKmhSw.setPressedState(LOW);

    maxTempBtn.setPressedState(LOW);
    baroUnitsSw.setPressedState(LOW);

    windChillBtn.setPressedState(LOW);
    tempUnitsSw.setPressedState(LOW);

    minBaroBtn.setPressedState(LOW);
    timeStartSw.setPressedState(LOW);

    maxBaroBtn.setPressedState(LOW);
    tempAutoHoldSw.setPressedState(LOW);

    baroRateBtn.setPressedState(LOW);
    hrMthSw.setPressedState(LOW);

    peakGustBtn.setPressedState(LOW);
    minDaySw.setPressedState(LOW);

    windAvgBtn.setPressedState(LOW);
    timeAutoHoldSw.setPressedState(LOW);

    railwayTimeSw.setPressedState(LOW);
}

/**
 * @brief Updates a button based on its current intex
 *
 * @param idx
 */
void updateButton(uint8_t idx)
{
    // This could be replaced later with a cleaner mapping but for now this works
    switch (idx)
    {
    case 4:
        maxTempBtn.update();
        baroUnitsSw.update();
        break;

    case 5:
        minBaroBtn.update();
        timeStartSw.update();
        break;

    case 6:
        windChillBtn.update();
        tempUnitsSw.update();
        break;

    case 7:
        maxBaroBtn.update();
        tempAutoHoldSw.update();
        break;

    case 9:
        clearBtn.update();
        windSpeedKnotSw.update();
        break;

    case 10:
        railwayTimeSw.update();
        break;

    case 11:
        minTempBtn.update();
        windSpeedKmhSw.update();
        break;

    case 12:
        baroRateBtn.update();
        hrMthSw.update();
        break;

    case 13:
        windAvgBtn.update();
        timeAutoHoldSw.update();
        break;

    case 14:
        peakGustBtn.update();
        minDaySw.update();
        break;

        // case 15:
        //     Serial.print("\n");
        //     break;

    default:
        break;
    }

    // Serial.print(idx);
    // Serial.print(":");
    // Serial.print(digitalRead(BACK_INPUT_SEL));
    // Serial.print(" ");
}

/**
 * @brief Updates every button, switching from front to back when appropriate.
 *
 * @note This should be called at the start of every loop
 *
 */
[[deprecated("Replaced by method updateButton")]] void updateButtons()
{
    // Front
    digitalWrite(FRONT_INPUT_SEL, HIGH);
    digitalWrite(BACK_INPUT_SEL, LOW);

    // Scanout

    maxTempBtn.update();
    windChillBtn.update();
    minBaroBtn.update();
    maxBaroBtn.update();
    baroRateBtn.update();
    peakGustBtn.update();
    windAvgBtn.update();

    // Back
    digitalWrite(FRONT_INPUT_SEL, LOW);
    digitalWrite(BACK_INPUT_SEL, HIGH);

    delayMicroseconds(5);

    // Scanout
    windSpeedKnotSw.update();
    windSpeedKmhSw.update();
    baroUnitsSw.update();
    tempUnitsSw.update();
    timeStartSw.update();
    tempAutoHoldSw.update();
    hrMthSw.update();
    minDaySw.update();
    timeAutoHoldSw.update();
}
