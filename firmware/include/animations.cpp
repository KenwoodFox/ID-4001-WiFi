/**
 * @file animations.cpp
 * @author Joe (joe.sedutto@silvertech.com)
 * @brief A collection of little animations for the ID-4001-WiFi project
 * @date 2022-05-10
 *
 * @copyright Copyright (c) 2022
 *
 */

// Arduino
#include "Arduino.h"

/**
 * @brief Simple little spinning compass startup animation
 *
 */
void startupAnimation1(Compass compass)
{
    // Little startup animation
    for (int seg = 0; seg <= 17; seg++)
    {
        unsigned long now = millis();

        while (millis() < now + 50)
        {
            for (int i = 0; i <= seg; i++)
            {
                compass.setHeading(i * 22);
            }
        }
    }
}
