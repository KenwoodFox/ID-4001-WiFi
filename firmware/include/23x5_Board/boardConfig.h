#ifndef _BOARD_CONFIG_H
#define _BOARD_CONFIG_H

// Segments
const uint8_t SS_A = 26;
const uint8_t SS_B = 27;
const uint8_t SS_C = 14;
const uint8_t SS_D = 12;
const uint8_t LATCH = 22;

// Buttons
const uint8_t FRONT_INPUT_SEL = 13;
const uint8_t BACK_INPUT_SEL = 9;

// const uint8_t WindChillButtonPin = 27;
// const uint8_t MaxTempButtonPin = 26;
// const uint8_t BaroMaxButtonPin = 29;
// const uint8_t BaroMinButtonPin = 28;
// const uint8_t MinTempButtonPin = 31;
// const uint8_t ClearButtonPin = 30;
// // 12/24 hr
// const uint8_t WindAverageButtonPin = 32;
// const uint8_t BaroRateButtonPin = 35;
// const uint8_t PeakGustButtonPin = 34;

const uint8_t BUTTON_BOUNCE_INTERVAL = 5;

#endif
