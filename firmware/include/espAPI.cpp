/**
 * @file espAPI.cpp
 * @author Joe (joe.sedutto@silvertech.com)
 * @brief Functions and methods to interact with the esp32 api slave
 *
 * @copyright Copyright (c) 2022
 */

#include "Wire.h"
#include "WireSlaveRequest.h"
#include "RunningAverage.h"
#include "Arduino.h"

// Const
const byte esp_slave_addr = 0x04;
const byte max_slave_response = 32;
const uint32_t ms_per_hr = 3600000;
const uint32_t ms_per_min = 60000;

// Objects
WireSlaveRequest slaveReq(Wire, esp_slave_addr, max_slave_response);

// Values
String errorKey;
int errorCode;
bool baroTrend; // True for up trend, false for down
float baroMillibar, airTempC, windSpeedKmh;
int windHeading;

// Stats
byte avgSize = 32;
RunningAverage windspeedAvg(avgSize);
RunningAverage tempAvg(avgSize);
RunningAverage baroAvg(avgSize);
// Special extra number to keep track of the long term change
float baroLastHour = 1000.0;

enum Stat
{
    nul,
    min,
    max,
    avg,
    alt,
};

/**
 * @brief Setup for the ESP api
 *
 */
void beginESP()
{
    // Begin wire
    Wire.begin();
}

String getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = {0, -1};
    int maxIndex = data.length();

    for (int i = 0; i <= maxIndex && found <= index; i++)
    {
        if (data.charAt(i) == separator || i == maxIndex)
        {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i + 1 : i;
        }
    }

    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

bool getNewPacket()
{
    // Some periodic statistic functions
    // Update hourly
    if (millis() % ms_per_hr < 1000)
    {
        baroLastHour = baroAvg.getAverage();
#ifndef DEBUG
        Serial.println("Set Hourly baro avg!");
#endif
    }

    // Attempt reading a new packet
    bool success = slaveReq.request(); // This method takes ~40ms!!!!!!!!!!
    String i2cInString;
    if (success)
    {
        while (1 < slaveReq.available())
        {                             // loop through all but the last byte
            char c = slaveReq.read(); // receive byte as a character
                                      //              Serial.print(c);                // print the character
            i2cInString += c;
        }

        // Normal Values
        // NOTE: These could perhaps be removed from the global scope...
        errorKey = getValue(i2cInString, '|', 0);
        errorCode = getValue(i2cInString, '|', 1).toInt();
        baroMillibar = getValue(i2cInString, '|', 3).toFloat();
        airTempC = getValue(i2cInString, '|', 4).toFloat();
        windSpeedKmh = getValue(i2cInString, '|', 5).toFloat();
        windHeading = getValue(i2cInString, '|', 6).toInt();

        // Stats
        windspeedAvg.addValue(windSpeedKmh);
        tempAvg.addValue(airTempC);
        baroAvg.addValue(baroMillibar);

        //
        if (getValue(i2cInString, '|', 2) == "falling")
        {
            baroTrend = true;
        }
        else
        {
            baroTrend = false;
        }
    }
    else
    {
#ifndef DEBUG
        Serial.print("\x1B[31mi^2c Error: ");
        Serial.println(slaveReq.lastStatusToString());
        Serial.print("\x1B[0m");
#endif
        display.setErr(i2cError);
    }
}

/**
 * ===========================
 * Methods to retrive i2c vars
 * ===========================
 */
float average(float *arr, int len)
{

    long sum = 0L; // sum will be larger than an item, long for safety.
    for (int i = 0; i < len; i++)
        sum += arr[i];
    return ((float)sum) / len; // average will be fractional, so float may be appropriate.
}

float getWindspeed(Stat _stat = nul)
{
    switch (_stat)
    {
    case avg:
        return windspeedAvg.getAverage();

    case max:
        return windspeedAvg.getMax();

    default:
        return windSpeedKmh;
    }
}

float getTemp(Stat _stat = nul)
{
    switch (_stat)
    {
    case avg:
        return tempAvg.getAverage();

    case max:
        return tempAvg.getMax();

    case min:
        return tempAvg.getMin();

    case alt:
        double windchill;

        windchill = 13.12 + (0.6215 * tempAvg.getAverage()) - (11.37 * (pow(windspeedAvg.getAverage(), 0.16))) + (0.3965 * tempAvg.getAverage() * (pow(windspeedAvg.getAverage(), 0.16)));

        if (windchill > tempAvg.getAverage() /*|| tempAvg.getAverage() > 10 || windspeedAvg.getAverage() < 5*/)
        {
            return 210;
        }
        else
        {
            return int(windchill);
        }

    default:
        return airTempC;
    }
}

float getBaro(Stat _stat = nul)
{
    switch (_stat)
    {
    case max:
        return baroAvg.getMax();

    case min:
        return baroAvg.getMin();

    case alt:
        // Gets the hr rate
        // Funky but should always return the oldest index and get it "baroAvg.getElement((baroAvg._index + 1) % baroAvg.getSize())" Dosent work because protected index
        return baroAvg.getAverage() - baroLastHour;

    default:
        return baroMillibar;
    }
}

void clearStats()
{
    windspeedAvg.clear();
    baroAvg.getAverage();
    tempAvg.clear();
}
