/**
 * @file errors.h
 * @author Joe (joe.sedutto@silvertech.com)
 * @brief Enumerators and other Data Types + docs for the ID-4001-WiFi error handling and reporting system
 * @date 2022-05-10
 *
 * @copyright Copyright (c) 2022
 *
 */

/**
 * @brief Represents all possible id-4001-wifi errors
 *
 */
enum Error
{
    /**
     * @brief Err00: Only used for testing.
     *
     */
    noErr = 0,

    /**
     * @brief Err01: No communication (Ususally refering to communication with ESP32 or API server).
     *
     */
    noComm = 1,

    /**
     * @brief Err02: Feature is not implemented or not yet finished. (May trigger on unconfigured buttons/config settings).
     *
     */
    notImplemented = 2,

    /**
     * @brief Err03: Function produced numbers out of range or function is not applicable to the current range (ie, rate when there is no deviation data).
     *
     */
    outOfRange = 3,

    /**
     * @brief Err04: GPS error, usually encoding failure.
     *
     */
    gpsError = 4,

    /**
     * @brief Err05: i^2c error, ususally decoding failure.
     *
     */
    i2cError = 5,
};
