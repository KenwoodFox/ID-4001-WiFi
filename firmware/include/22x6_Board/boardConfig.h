#ifndef _BOARD_CONFIG_H
#define _BOARD_CONFIG_H

// Pins TODO: Move
const uint8_t WINDA_PIN = 23;
const uint8_t WINDB_PIN = 25;
const uint8_t WINDC_PIN = 27;
const uint8_t WINDD_PIN = 29;

const uint8_t SS_A = 46;
const uint8_t SS_B = 48;
const uint8_t SS_C = 42;
const uint8_t SS_D = 44;
const uint8_t S_DP = 26;
const uint8_t S_A = 28;
const uint8_t S_B = 30;
const uint8_t S_C = 32;
const uint8_t S_D = 34;
const uint8_t S_E = 36;
const uint8_t S_F = 38;
const uint8_t S_G = 40;

#endif
