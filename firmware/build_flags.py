import logging
import subprocess

from datetime import date


today = date.today()

try:
    # Revision
    revision = (
        subprocess.check_output(
            [
                "git",
                "describe",
                "--abbrev=8",
                "--dirty",
                "--always",
                "--tags",
            ]
        )
        .strip()
        .decode("utf-8")
    )

    print(f"-D FWREVISION='\"{revision}\"'")
    print(f"-D FWREVISION_YEAR='{int(revision.split('.')[0][1:])}'")  # string indices
    print(f"-D FWREVISION_MONTH='{int(revision.split('.')[1][:2])}'")
    try:
        print(f"-D FWREVISION_VER='{int(revision.split('-')[1])}'")
    except:
        print(f"-D FWREVISION_VER='{0}'")
except Exception as e:
    logging.warning("Getting git revision failed!! Check that you have git installed?")
    logging.warning(e)

    print("-D FWREVISION='Unknown'")
    print("-D FWREVISION_YEAR='00'")
    print("-D FWREVISION_MONTH='12'")
    print("-D FWREVISION_VER='0'")

try:
    host = (
        subprocess.check_output(
            [
                "hostname",
            ]
        )
        .strip()
        .decode("utf-8")
    )

    print(f"-D FWHOST='\"{host}\"'")
except Exception as e:
    logging.warning("Getting host failed!! Are you using a UNIX compatible system?")
    logging.warning(e)

    print("-D FWHOST='Unknown'")

try:
    username = (
        subprocess.check_output(
            [
                "id",
                "-u",
                "-n",
            ]
        )
        .strip()
        .decode("utf-8")
    )

    # Cleanup CI
    if username == "root":
        username = "gitlab"
        host = "gitlab"

    print(f"-D USERNAME='\"{username}\"'")
except Exception as e:
    logging.warning("Getting host failed!! Are you using a UNIX compatible system?")
    logging.warning(e)

    print("-D USERNAME='Unknown'")


# Print to enable debugging
print("-D DEBUG")

# This is for the board config
print("-I include/23x5_Board")
