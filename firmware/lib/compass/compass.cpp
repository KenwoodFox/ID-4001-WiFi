/**
 * @file compass.cpp
 * @author Joe (joe.sedutto@silvertech.com)
 * @brief Methods to control, update and use the ID-4001 native compass.
 * @date 2022-05-10
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "compass.h"
#include "Arduino.h"

#define countof(a) (sizeof(a) / sizeof(*(a)))

Compass::Compass(byte _WINDA_PIN, byte _WINDB_PIN, byte _WINDC_PIN, byte _WINDD_PIN)
{
    // Save all pins to local memory
    WINDA_PIN = _WINDA_PIN;
    WINDB_PIN = _WINDB_PIN;
    WINDC_PIN = _WINDC_PIN;
    WINDD_PIN = _WINDD_PIN;

    // Setup pins
    pinMode(WINDA_PIN, OUTPUT);
    pinMode(WINDB_PIN, OUTPUT);
    pinMode(WINDC_PIN, OUTPUT);
    pinMode(WINDD_PIN, OUTPUT);
};

void Compass::setHeading(int angle)
{
    // Map vars
    angle = angle + 360 - 180 / countof(roseLookup); // Round angles
    int index = angle * countof(roseLookup) / 360;   // Map index
    index = (index + 1) % countof(roseLookup);       // Loop back

    digitalWrite(WINDA_PIN, bitRead(roseLookup[index], 3));
    digitalWrite(WINDB_PIN, bitRead(roseLookup[index], 2));
    digitalWrite(WINDC_PIN, bitRead(roseLookup[index], 1));
    digitalWrite(WINDD_PIN, bitRead(roseLookup[index], 0));
};

void Compass::iterate(int angle)
{
    // Store
    if (windDirAvgWriteIdx > compassTrail)
    {
        windDirAvgWriteIdx = 0;
    }

    if (windDirAvg[windDirAvgWriteIdx - 1] != angle)
    {
        windDirAvg[windDirAvgWriteIdx] = angle;
        windDirAvgWriteIdx++;
    }

    // Draw
    if (windDirAvgReadIdx > compassTrail)
    {
        windDirAvgReadIdx = 0;
    }

    setHeading(windDirAvg[windDirAvgReadIdx]);
    windDirAvgReadIdx++;
}

void Compass::clearStats()
{
    memset(windDirAvg, 0.0, sizeof(windDirAvg));
}
