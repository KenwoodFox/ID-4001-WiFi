# Compass Lib

![Compass Closeup](resources/compass.jpg)


# Direction Mapping

This was a bit of a pain to work out...

```
0b0001111 N*
0b0001101 NNE*
0b0000101 NE
0b0000001 ENE*
0b0001001 E*
0b0001000 ESE*
0b0000000 SE*
0b0000100 SSE*
0b0001100 S*
0b0001110 SSW*
0b0000110 SW*
0b0000010 WSW*
0b0001010 W*
0b0001011 WNW*
0b0000011 NW*
0b0000111 NNW*
```
