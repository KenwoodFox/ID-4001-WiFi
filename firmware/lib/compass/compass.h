/**
 * @file compass.h
 * @author Joe (joe.sedutto@silvertech.com)
 * @brief Data and Prototypes for the ID-4001-WiFi Compass
 * @date 2022-05-10
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "Arduino.h"

/**
 * @brief Compass class used to manipulate the id-4001 compass.
 *
 * @author Joe (joe.sedutto@silvertech.com)
 */
class Compass
{
private:
    byte WINDA_PIN;
    byte WINDB_PIN;
    byte WINDC_PIN;
    byte WINDD_PIN;

    // Lookup table
    const byte roseLookup[16] = {
        0b0001111, // N
        0b0001101, // NNE
        0b0000101, // NE
        0b0000001, // ENE
        0b0001001, // E
        0b0001000, // ESE
        0b0000000, // SE
        0b0000100, // SSE
        0b0001100, // S
        0b0001110, // SSW
        0b0000110, // SW
        0b0000010, // WSW
        0b0001010, // W
        0b0001011, // WNW
        0b0000011, // NW
        0b0000111, // NNW
    };

    const int degInCompass = 360;
    const static byte compassTrail = 5;
    /**
     * @brief This is neat, it stores the last 10 wind directions to avg them
     *
     */
    int windDirAvg[compassTrail + 1];
    byte windDirAvgReadIdx = 0;
    byte windDirAvgWriteIdx = 0;

public:
    /**
     * @brief Construct a new Compass object
     *
     * @param _WINDA_PIN Wind "A" pin.
     * @param _WINDB_PIN Wind "B" pin.
     * @param _WINDC_PIN Wind "C" pin.
     * @param _WINDD_PIN Wind "D" pin.
     */
    [[deprecated("Replaced by methods in display.h")]] Compass(byte _WINDA_PIN, byte _WINDB_PIN, byte _WINDC_PIN, byte _WINDD_PIN);

    /**
     * @brief Set the output heading
     *
     * @param angle In degrees, 0-360
     */
    [[deprecated("Replaced by methods in display.h")]] void setHeading(int angle);

    /**
     * @brief Like Display's iterate function, this method expects you to do the scheduling on your own.
     *
     */
    [[deprecated("Replaced by methods in display.h")]] void iterate(int angle);

    /**
     * @brief Simple method to clear/reset all running stats.
     *
     */
    [[deprecated("Replaced by methods in display.h")]] void clearStats();
};
