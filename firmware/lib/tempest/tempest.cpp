/**
 * @file tempest.h
 * @author Joe (joe.sedutto@silvertech.com)
 * @brief Methods and objects to allow communication with a tempest weather station
 *
 */

#include "tempest.h"
#include "ArduinoJson.h"

Tempest::Tempest() : client(wifi, "ws.weatherflow.com")
{
}

void Tempest::refreshData()
{
    if (!client.connected())
    {
        client.begin("/swd/data?token=e12379e4-8434-4ef6-877e-ce893154882f"); // Reconnect
        return;
    }

    // We need to send a little json over to signal we want some data
    StaticJsonDocument<2000> cmd;
    cmd["type"] = "listen_start";
    cmd["device_id"] = "23346";
    cmd["id"] = "12345";

    client.beginMessage(TYPE_TEXT);
    serializeJson(cmd, client);
    client.endMessage();

    while (client.connected())
    {
        // check if a message is available to be received
        int messageSize = client.parseMessage();
        if (messageSize > 0)
        {
            String m = client.readString();
            count += 1;
            Serial.print(count);
            Serial.print(" : ");
            Serial.println(m);
        }
    }

    // Make an object of correct size to deserialize
    DynamicJsonDocument doc(capacity); // Store doc on the heap

    // Attempt to deserialize the payload
    DeserializationError err = deserializeJson(doc, exampleJson);

    if (err)
    {
        // Handle warning of JSON fault here.
        Serial.print("Json error! ");
        Serial.println(err.c_str());
    }
    else
    {
        /*
         * Parse JSON
         */

        // Status and status codes
        status_code = doc["status"]["status_code"];       // 0
        status_message = doc["status"]["status_message"]; // "SUCCESS"

        // Observation data
        JsonObject obs = doc["obs"][0];
        obs_timestamp = obs["timestamp"];                 // ?
        obs_wind_lull = obs["wind_lull"];                 // 0.5
        obs_wind_avg = obs["wind_avg"];                   // 0.8
        obs_wind_gust = obs["wind_gust"];                 // 1
        obs_wind_dir = obs["wind_direction"];             // 219
        barometric_pressure = obs["barometric_pressure"]; // 1013.5
        air_temperature = obs["air_temperature"];         // 15

        // Serial.println(status_message);
    }
}
