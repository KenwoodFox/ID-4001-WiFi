/**
 * @file tempest.h
 * @author Joe (joe.sedutto@silvertech.com)
 * @brief Methods and objects to allow communication with a tempest weather station
 *
 */

#include "Arduino.h"
#include <WiFiNINA.h>
#include <WiFiUdp.h>
#include <ArduinoJson.h>
#include <ArduinoHttpClient.h>

class Tempest
{
private:
    // Wifi
    WiFiClient wifi;
    WebSocketClient client;

    // Config
    char token[40] = "e12379e4-8434-4ef6-877e-ce893154882f";
    char station_id[10] = "23346";
    char user_id[10] = "12345"; // Maybe not needed?

    // Json memory
    // define json document with correct capacity
    const int capacity = JSON_ARRAY_SIZE(1) + JSON_ARRAY_SIZE(32) + JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(7) + JSON_OBJECT_SIZE(12) + JSON_OBJECT_SIZE(35) + 1110;
    int count = 0;

    // Testing and other non-required memory
    const char *exampleJson = "{\"station_id\":23346,\"station_name\":\"Wendover Way\",\"public_name\":\"Wendover Way\",\"latitude\":42.95757,\"longitude\":-71.49607,\"timezone\":\"America/New_York\",\"elevation\":69.34413146972656,\"is_public\":true,\"status\":{\"status_code\":0,\"status_message\":\"SUCCESS\"},\"station_units\":{\"units_temp\":\"f\",\"units_wind\":\"mph\",\"units_precip\":\"in\",\"units_pressure\":\"inhg\",\"units_distance\":\"mi\",\"units_direction\":\"cardinal\",\"units_other\":\"imperial\"},\"outdoor_keys\":[\"timestamp\",\"air_temperature\",\"barometric_pressure\",\"station_pressure\",\"pressure_trend\",\"sea_level_pressure\",\"relative_humidity\",\"precip\",\"precip_accum_last_1hr\",\"precip_accum_local_day\",\"precip_accum_local_yesterday_final\",\"precip_minutes_local_day\",\"precip_minutes_local_yesterday_final\",\"wind_avg\",\"wind_direction\",\"wind_gust\",\"wind_lull\",\"solar_radiation\",\"uv\",\"brightness\",\"lightning_strike_last_epoch\",\"lightning_strike_last_distance\",\"lightning_strike_count\",\"lightning_strike_count_last_1hr\",\"lightning_strike_count_last_3hr\",\"feels_like\",\"heat_index\",\"wind_chill\",\"dew_point\",\"wet_bulb_temperature\",\"delta_t\",\"air_density\"],\"obs\":[{\"timestamp\":1601846842,\"air_temperature\":15,\"barometric_pressure\":1013.5,\"station_pressure\":1013.5,\"sea_level_pressure\":1022.3,\"relative_humidity\":64,\"precip\":0,\"precip_accum_last_1hr\":0,\"precip_accum_local_day\":0,\"precip_accum_local_yesterday\":0,\"precip_accum_local_yesterday_final\":0,\"precip_minutes_local_day\":0,\"precip_minutes_local_yesterday\":0,\"precip_minutes_local_yesterday_final\":0,\"precip_analysis_type_yesterday\":1,\"wind_avg\":0.8,\"wind_direction\":219,\"wind_gust\":1,\"wind_lull\":0.5,\"solar_radiation\":40,\"uv\":0.13,\"brightness\":4798,\"lightning_strike_last_epoch\":1599771905,\"lightning_strike_last_distance\":41,\"lightning_strike_count\":0,\"lightning_strike_count_last_1hr\":0,\"lightning_strike_count_last_3hr\":0,\"feels_like\":15,\"heat_index\":15,\"wind_chill\":15,\"dew_point\":8.2,\"wet_bulb_temperature\":11.3,\"delta_t\":3.7,\"air_density\":1.22528,\"pressure_trend\":\"steady\"}]}";

public:
    // Returnable Memory
    char station_name[20] = "";
    unsigned long int timestamp = 0; // Defaults to 0 (or, Jan 1, 1970 00:00)

    // Status
    int status_code = 1;        // 0 indicates success
    const char *status_message; // Status message

    // Station information
    const char *serial_number; // Serial number of station

    // Observations
    long obs_timestamp;
    float obs_wind_lull;
    float obs_wind_avg;
    float barometric_pressure;
    int air_temperature;
    int obs_wind_gust;
    int obs_wind_dir;

    /**
     * @brief Construct a new Tempest interface object
     *
     */
    Tempest();

    /**
     * @brief Refresh the data coming into the tempest
     *
     */
    void refreshData();
};
