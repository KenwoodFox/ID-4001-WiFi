/**
 * @file display.h
 * @author Joe (joe.sedutto@silvertech.com)
 * @brief Data and Prototypes for the ID-4001-WiFi Display
 * @date 2022-05-10
 *
 * @copyright Copyright (c) 2022
 *
 */

#include "Arduino.h"

/**
 * @brief Display object used to manipulate the display.
 *
 * @author Joe (joe.sedutto@silvertech.com)
 */
class Display
{
private:
    uint8_t SEG_SEL_A_PIN;
    uint8_t SEG_SEL_B_PIN;
    uint8_t SEG_SEL_C_PIN;
    uint8_t SEG_SEL_D_PIN;

    uint8_t LATCH_PIN;

    uint8_t SEG_DP_PIN;

    uint8_t SEG_A_PIN;
    uint8_t SEG_B_PIN;
    uint8_t SEG_C_PIN;
    uint8_t SEG_D_PIN;
    uint8_t SEG_E_PIN;
    uint8_t SEG_F_PIN;
    uint8_t SEG_G_PIN;

    // Segments
    enum Seg
    {
        s_A = 0b00000001,
        s_B = 0b00000010,
        s_C = 0b00000100,
        s_D = 0b00001000,
        s_E = 0b00010000,
        s_F = 0b00100000,
        s_G = 0b01000000,
        s_DP = 0b10000000,
    };

    // These apply only for the truly 8 digit segments, see special cases below
    const uint8_t segmentLookup[37] = {
        s_A | s_B | s_C | s_D | s_E | s_F,       // 0
        s_B | s_C,                               // 1
        s_A | s_B | s_D | s_E | s_G,             // 2
        s_A | s_B | s_C | s_D | s_G,             // 3
        s_B | s_C | s_F | s_G,                   // 4
        s_A | s_C | s_D | s_F | s_G,             // 5
        s_A | s_C | s_D | s_E | s_F | s_G,       // 6
        s_A | s_B | s_C,                         // 7
        s_A | s_B | s_C | s_D | s_E | s_F | s_G, // 8
        s_A | s_B | s_C | s_D | s_F | s_G,       // 9
        0,                                       // null
        s_A | s_B | s_C | s_E | s_F | s_G,       // A
        s_C | s_D | s_E | s_F | s_G,             // b
        s_A | s_D | s_E | s_F,                   // C
        s_B | s_C | s_D | s_E | s_G,             // d
        s_A | s_D | s_E | s_F | s_G,             // E
        s_A | s_E | s_F | s_G,                   // F
        s_A | s_C | s_D | s_E | s_F,             // G
        s_C | s_E | s_F | s_G,                   // h
        s_B | s_C,                               // i
        s_B | s_C | s_D,                         // j
        s_A | s_C | s_E | s_F | s_G,             // k
        s_D | s_E | s_F,                         // L
        s_A | s_C | s_E,                         // M
        s_A | s_B | s_C | s_E | s_F,             // N
        s_A | s_B | s_C | s_D | s_E | s_F,       // O
        s_A | s_B | s_E | s_F | s_G,             // p
        s_A | s_B | s_C | s_F | s_G,             // q
        s_E | s_G,                               // r
        s_A | s_C | s_D | s_F | s_G,             // s
        s_D | s_E | s_F | s_G,                   // t
        s_B | s_C | s_D | s_E | s_F,             // U
        s_B | s_C | s_D | s_F,                   // V
        s_B | s_D | s_F,                         // W
        s_B | s_C | s_E | s_F | s_G,             // X
        s_B | s_C | s_D | s_F | s_G,             // Y
        s_A | s_B | s_D | s_G,                   // Z
    };

    // These are custom, because the baro indicator uses the 6th segment for rising
    const uint8_t baroSegmentLookup[5] = {
        0,                           // 0 (cant display)
        s_B | s_C,                   // 1
        s_A | s_B | s_D | s_E | s_G, // 2
        s_A | s_B | s_C | s_D | s_G, // 3
        0                            // null
    };

    // The first digit in the temp display is also kinda custom and must use this map instead.
    const uint8_t tempSegmentLookup[6] = {
        0,                           // 0 (Cant display)
        s_A | s_B,                   // 1
        s_A | s_B | s_C | s_D | s_E, // +1
        s_E,                         // -
        s_B | s_C | s_E,             // -1
        s_C | s_D | s_E,             // +
    };

    // The first digit in the time display has custom mappings as well
    const uint8_t timeSegmentLookup[11] = {
        0,                           // 0 (Cant display)
        s_B | s_C,                   // 1
        s_A | s_B | s_D | s_E | s_G, // 2
        s_A | s_B | s_C | s_D | s_G, // 3
        0,                           // 4 (Cant display)
        0,                           // 5 (Cant display)
        0,                           // 6 (Cant display)
        s_A | s_B | s_C,             // 7
        0,                           // 8 (Cant display)
        0,                           // 9 (Cant display)
        0                            // null
    };

    // Lookup table for compass display
    const byte roseLookup[16] = {
        0b00001111, // N
        0b00001011, // NNE
        0b00001010, // NE
        0b00001000, // ENE
        0b00001001, // E
        0b00000001, // ESE
        0b00000000, // SE
        0b00000010, // SSE
        0b00000011, // S
        0b00000111, // SSW
        0b00000110, // SW
        0b00000100, // WSW
        0b00000101, // W
        0b00001101, // WNW
        0b00001100, // NW
        0b00001101, // NNW
    };

    // Each multi-segment indicator section, arranged in order
    uint8_t timeDigits[6] = {10, 14, 9, 13, 11, 15};
    uint8_t windDigits[2] = {8, 12};
    uint8_t tempDigits[3] = {4, 2, 6};
    uint8_t baroDigits[4] = {1, 5, 3, 7};
    uint8_t allDigits[15] = {10, 14, 9, 13, 11, 15, 8, 12, 4, 2, 6, 1, 5, 3, 7};

    // Single high speed array to address when updating new values
    uint8_t displayDigits[16] = {
        0b0000000, // Never used?
        0b0000000,
        0b0000000,
        0b0000000,
        0b0000000,
        0b0000000,
        0b0000000, // Never used?
        0b0000000,
        0b0000000,
        0b0000000,
        0b0000000,
        0b0000000,
        0b0000000,
        0b0000000,
        0b0000000,
        0b0000000,
    };

    // Current compass position
    uint8_t displayCompass = 0;
    // Compass fill
    bool clear = false;    // Whether or not we're in clearing mode
    bool fill = false;     // Whether or not to fill the compass when drawing.
    uint8_t lastFill = 0;  // Stores whatever the last fill index was
    uint8_t fillWidth = 0; // Stores the current fill width (if using)

    /**
     * @brief Return the n'th digit in an int (base 10)
     *
     * @note Do not confuse with bitRead(): Returning the n'th bit in an int (base 2)
     *
     * @param val An int value
     * @param n What power to extract, eg 0 for `1` in `1234`. and 2 for `3` in `1234`
     * @return The digit extracted (0-9)
     */
    int nth_digit(int val, int n);
    unsigned int pows[5] = {1, 10, 100, 1000, 10000};

    /**
     * @brief Checks to see if a set amount of time has elapsed.
     *
     * @param time The time in millis() to reference from
     * @param dur A duration (in seconds)
     * @return true if time has expired
     * @return false if time has not yet expired
     */
    bool hasElapsed(unsigned long time, int dur);
    uint8_t ALT_DECAY = 5;     // Natural decay of alt info screens (in seconds)
    unsigned long lastTimeAlt; // Millis time when we displayed the previous alt item in the time box

    /**
     * @brief Writes a specific value to a specific LED index
     *
     * @param value The value to write @see segmentLookup
     * @param index The index of the segment to display on
     * @param drawZeros Draw zeros or not
     */
    void writeRawDigit(uint8_t value, uint8_t index, bool drawZeros);

    // Used for iterate refresh method
    uint8_t globSegIdx;

public:
    enum Letter
    {
        l_BLANK = 10,
        l_A = 11,
        l_B = 12,
        l_C = 13,
        l_D = 14,
        l_E = 15,
        l_F = 16,
        l_G = 17,
        l_H = 18,
        l_I = 19,
        l_J = 20,
        l_K = 21,
        l_L = 22,
        l_M = 23,
        l_N = 24,
        l_O = 25,
        l_P = 26,
        l_Q = 27,
        l_R = 28,
        l_S = 29,
        l_T = 30,
        l_U = 31,
        l_V = 32,
        l_W = 33,
        l_X = 34,
        l_Y = 35,
        l_Z = 36,
    };

    /**
     * @brief Constructs a new Display object
     *
     * @param _SEG_SEL_A_PIN Segment Selector (A)
     * @param _SEG_SEL_B_PIN Segment Selector (B)
     * @param _SEG_SEL_C_PIN Segment Selector (C)
     * @param _SEG_SEL_D_PIN Segment Selector (D)
     * @param _SEG_DP_PIN Segment Decimal Point pin
     * @param _SEG_A_PIN Segment "A" LED (top)
     * @param _SEG_B_PIN Segment "B" LED (top right)
     * @param _SEG_C_PIN Segment "C" LED (bottom right)
     * @param _SEG_D_PIN Segment "D" LED (bottom)
     * @param _SEG_E_PIN Segment "E" LED (bottom left)
     * @param _SEG_F_PIN Segment "F" LED (top left)
     * @param _SEG_G_PIN Segment "G" LED (center)
     */
    [[deprecated("Replaced by constructor using SPI")]] Display(uint8_t _SEG_SEL_A_PIN, uint8_t _SEG_SEL_B_PIN, uint8_t _SEG_SEL_C_PIN, uint8_t _SEG_SEL_D_PIN, uint8_t _SEG_DP_PIN, uint8_t _SEG_A_PIN, uint8_t _SEG_B_PIN, uint8_t _SEG_C_PIN, uint8_t _SEG_D_PIN, uint8_t _SEG_E_PIN, uint8_t _SEG_F_PIN, uint8_t _SEG_G_PIN);

    /**
     * @brief Construct a new Display object
     *
     * @param _SEG_SEL_A_PIN Segment Selector (A)
     * @param _SEG_SEL_B_PIN Segment Selector (B)
     * @param _SEG_SEL_C_PIN Segment Selector (C)
     * @param _SEG_SEL_D_PIN Segment Selector (D)
     * @param _LATCH_PIN     Segment Latch
     */
    Display(uint8_t _SEG_SEL_A_PIN, uint8_t _SEG_SEL_B_PIN, uint8_t _SEG_SEL_C_PIN, uint8_t _SEG_SEL_D_PIN, uint8_t _LATCH_PIN);

    /**
     * @brief Begin the display
     *
     */
    void begin();

    /**
     * @brief Set barometric pressure
     *
     * @param pres Pressure in Millibar
     * @param rising  Wether or not the pressure has been trending up.
     * @param falling Wether or not the pressure has been trending down.
     * @param autoscale If true, will autoscale the number to include the highest precision (move dp)
     */
    void setBaro(float pres, bool rising = true, bool falling = true, bool autoscale = true);

    /**
     * @brief Set windspeed
     *
     * @param speed Windspeed in km/h
     * @note Speed can be a float! setWindspeed will auto-scale from 0-9.9, and from 10-99
     */
    void setWindspeed(float speed);

    /**
     * @brief Set the compass heading
     *
     * @param angle In degrees, 0-360
     */
    void setHeading(int angle);

    /**
     * @brief Enable or disable compass fill
     *
     * @param _fill Whether or not to fill
     */
    void setFill(bool _fill);

    /**
     * @brief Enables the display to go full-clear/test mode
     *
     * @param _clear true when clearing
     */
    void setClear(bool _clear);

    /**
     * @brief Set the width of compass fill
     *
     * @param _fillWidth How much to fill
     */
    void setFillWidth(int _fillWidth);

    /**
     * @brief Set the temperature
     *
     * @param temp A temperature in degrees C
     * @param indoor Whether or not this reading is indoors
     * @param outdoor Whether or not this reading is outdoors
     * @param drawSign Whether or not to force draw the sign
     */
    void setTemp(float temp, bool indoor = false, bool outdoor = false, bool drawSign = false);

    /**
     * @brief Set the display time section
     *
     * @param time Any char array with 6 chars exactly, the time display can also display the date, error codes and more
     * @param am AM lamp
     * @param pm PM lamp
     * @note The AM lamp and PM lamp can be enabled/disabled at the same time. They are not mutually exclusive.
     */
    void setTime(char *time, bool am = false, bool pm = false);

    /**
     * @brief Raise and display an error code
     *
     * @param err An error code, (0-99)
     * @see Error
     */
    void setErr(int err);

    /**
     * @brief Instantly clear an error from the display.
     *
     * @see Error
     */
    void clearDisplay();

    /**
     * @brief Refresh the screen
     *
     * @param scrTime Time to be spent displaying each segment, in microseconds
     * @deprecated Deprecated, recommended to use Display.iterate instead.
     */
    void refresh(unsigned int scrTime = 2000);

    /**
     * @brief Iterates through each display module at a time, designed to be called with a regular timer callback.
     *
     * @returns The current index
     */
    uint8_t iterate();

    /**
     * @brief Replacement for arduino std round, this is a little messy but its better than truncating
     * the numbers we round. An alternative would be to only accept integers and leave rounding to the user.
     *
     * @param in
     * @param precision
     * @return float
     */
    float fastRound(float in, int precision);

    /**
     * @brief Return a cyclic value
     *
     * @param i    Value to circularize
     * @param max  The max bound
     * @return int Returned Value
     */
    int cyclic(int i, int max);
};
