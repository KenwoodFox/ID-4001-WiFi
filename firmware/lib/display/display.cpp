/**
 * @file display.cpp
 * @author Joe (joe.sedutto@silvertech.com)
 * @brief Methods to control, update and use the ID-4001 native display.
 * @date 2022-05-10
 *
 * @copyright Copyright (c) 2022
 *
 */

#define countof(a) (sizeof(a) / sizeof(*(a)))

#include "display.h"
#include "Arduino.h"

#include <SPI.h>

Display::Display(uint8_t _SEG_SEL_A_PIN, uint8_t _SEG_SEL_B_PIN, uint8_t _SEG_SEL_C_PIN, uint8_t _SEG_SEL_D_PIN, uint8_t _LATCH_PIN)
{
    // Save all pins to local memory
    SEG_SEL_A_PIN = _SEG_SEL_A_PIN;
    SEG_SEL_B_PIN = _SEG_SEL_B_PIN;
    SEG_SEL_C_PIN = _SEG_SEL_C_PIN;
    SEG_SEL_D_PIN = _SEG_SEL_D_PIN;
    LATCH_PIN = _LATCH_PIN;

    // Setup pins
    pinMode(SEG_SEL_A_PIN, OUTPUT);
    pinMode(SEG_SEL_B_PIN, OUTPUT);
    pinMode(SEG_SEL_C_PIN, OUTPUT);
    pinMode(SEG_SEL_D_PIN, OUTPUT);
    pinMode(LATCH_PIN, OUTPUT);

    // SPI Must be setup externally!
};

Display::Display(uint8_t _SEG_SEL_A_PIN, uint8_t _SEG_SEL_B_PIN, uint8_t _SEG_SEL_C_PIN, uint8_t _SEG_SEL_D_PIN, uint8_t _SEG_DP_PIN, uint8_t _SEG_A_PIN, uint8_t _SEG_B_PIN, uint8_t _SEG_C_PIN, uint8_t _SEG_D_PIN, uint8_t _SEG_E_PIN, uint8_t _SEG_F_PIN, uint8_t _SEG_G_PIN)
{
    // Save all pins to local memory
    SEG_SEL_A_PIN = _SEG_SEL_A_PIN;
    SEG_SEL_B_PIN = _SEG_SEL_B_PIN;
    SEG_SEL_C_PIN = _SEG_SEL_C_PIN;
    SEG_SEL_D_PIN = _SEG_SEL_D_PIN;

    SEG_DP_PIN = _SEG_DP_PIN;

    SEG_A_PIN = _SEG_A_PIN;
    SEG_B_PIN = _SEG_B_PIN;
    SEG_C_PIN = _SEG_C_PIN;
    SEG_D_PIN = _SEG_D_PIN;
    SEG_E_PIN = _SEG_E_PIN;
    SEG_F_PIN = _SEG_F_PIN;
    SEG_G_PIN = _SEG_G_PIN;

    // Setup pins
    pinMode(SEG_SEL_A_PIN, OUTPUT);
    pinMode(SEG_SEL_B_PIN, OUTPUT);
    pinMode(SEG_SEL_C_PIN, OUTPUT);
    pinMode(SEG_SEL_D_PIN, OUTPUT);

    pinMode(SEG_DP_PIN, OUTPUT);
    pinMode(SEG_A_PIN, OUTPUT);
    pinMode(SEG_B_PIN, OUTPUT);
    pinMode(SEG_C_PIN, OUTPUT);
    pinMode(SEG_D_PIN, OUTPUT);
    pinMode(SEG_E_PIN, OUTPUT);
    pinMode(SEG_F_PIN, OUTPUT);
    pinMode(SEG_G_PIN, OUTPUT);
};

void Display::begin()
{
    // Setup SPI
    SPI.begin();
    SPISettings shiftRegisterSettings(800000, MSBFIRST, SPI_MODE0);
    SPI.beginTransaction(shiftRegisterSettings);
}

void Display::writeRawDigit(uint8_t value, uint8_t index, bool drawZeros = false)
{
    // Segment Selector
    digitalWrite(SEG_SEL_A_PIN, index & 0b00001000);
    digitalWrite(SEG_SEL_B_PIN, index & 0b00000100);
    digitalWrite(SEG_SEL_C_PIN, index & 0b00000010);
    digitalWrite(SEG_SEL_D_PIN, index & 0b00000001);

    // Begin write to Shift Reg
    digitalWrite(LATCH_PIN, LOW); // Stop display

    // Compass fill
    if (fill && fillWidth > 0)
    {
        // For filling from 0
        // SPI.transfer(roseLookup[lastFill]);
        // lastFill = ++lastFill % (displayCompass + 1);

        SPI.transfer(roseLookup[cyclic(lastFill + (displayCompass - (fillWidth / 2)) - 1, 16) + 1]);

        lastFill = ++lastFill % (fillWidth + 1);
    }
    else
    {
        SPI.transfer(roseLookup[displayCompass]); // Write the current compass
    }

    // Clearing mode
    if (!clear)
    {
        SPI.transfer(value); // Write value to SPI buffer
    }
    else
    {
        SPI.transfer(255); // Write clear bits to everything
    }
    digitalWrite(LATCH_PIN, HIGH); // Resume display
};

void Display::setWindspeed(float speed)
{
    speed = fastRound(speed, 1);

    if (speed >= 0 && speed < 10) // From 0 to 10
    {
        speed = speed * 10; // Scale

        displayDigits[windDigits[0]] = segmentLookup[Display::nth_digit(speed, 1)];
        displayDigits[windDigits[1]] = segmentLookup[Display::nth_digit(speed, 0)];

        bitSet(displayDigits[windDigits[0]], 7); // Set decimal point
        return;
    }
    else if (speed < 0 || speed >= 100) // Less than 0 or more than 100
    {
        displayDigits[windDigits[0]] = segmentLookup[l_E]; // E
        displayDigits[windDigits[1]] = segmentLookup[l_R]; // r
    }
    else // Between 10 and 100
    {
        displayDigits[windDigits[0]] = segmentLookup[Display::nth_digit(speed, 1)];
        displayDigits[windDigits[1]] = segmentLookup[Display::nth_digit(speed, 0)];
    }
};

void Display::setFill(bool _fill)
{
    fill = _fill;
}

void Display::setClear(bool _clear)
{
    clear = _clear;
}

void Display::setFillWidth(int _fillWidth)
{
    fillWidth = _fillWidth;
}

void Display::setHeading(int angle)
{
    // Map vars
    angle = angle + 360 - 180 / countof(roseLookup); // Round angles
    int index = angle * countof(roseLookup) / 360;   // Map index
    index = (index + 1) % countof(roseLookup);       // Loop back
    displayCompass = index;                          // Set the current display
}

void Display::setBaro(float pres, bool rising, bool falling, bool autoscale)
{

    if (pres >= 4000 || pres < 0) // Can't display numbers this big
    {
        displayDigits[baroDigits[0]] = baroSegmentLookup[4];
        displayDigits[baroDigits[1]] = segmentLookup[l_E];
        displayDigits[baroDigits[2]] = segmentLookup[l_R];
        displayDigits[baroDigits[3]] = segmentLookup[l_R];
        return;
    }

    // Can only display positive
    pres = abs(pres);

    // Treat mercury as a bigger number (todo, messy)
    if (pres < 100 && autoscale)
    {
        pres = pres * 100;
    }

    // First digit is *special*
    uint8_t firstSegment = baroSegmentLookup[Display::nth_digit(pres, 3)];
    if (falling)
    {
        firstSegment |= s_DP; // Flip DP bit
    }
    if (rising)
    {
        firstSegment |= s_F; // Flip F bit.
    }

    // First segment
    displayDigits[baroDigits[0]] = firstSegment;

    // Remaining segments
    displayDigits[baroDigits[1]] = segmentLookup[Display::nth_digit(pres, 2)];
    displayDigits[baroDigits[2]] = segmentLookup[Display::nth_digit(pres, 1)];
    displayDigits[baroDigits[3]] = segmentLookup[Display::nth_digit(pres, 0)];
};

void Display::setTemp(float temp, bool indoor, bool outdoor, bool drawSign)
{
    // Fast round
    temp = fastRound(temp, 0);

    // The first digit has a very special map
    // Cannot use switch here,
    if (temp <= -100)
    {
        displayDigits[tempDigits[0]] = tempSegmentLookup[4]; // Custom digit, "-1" char
    }
    else if (temp >= 0 && temp < 100)
    {
        // Between 0 and 100 degrees
        displayDigits[tempDigits[0]] = tempSegmentLookup[0]; // Custom digit, null char

        // Force drawing 100
        if (drawSign)
        {
            displayDigits[tempDigits[0]] = tempSegmentLookup[5]; // Custom digit, + char
        }
    }
    else if (temp < 0)
    {
        displayDigits[tempDigits[0]] = tempSegmentLookup[3]; // Custom digit, "-" char
    }
    else if (temp >= 200)
    {
        // Cant draw numbers this big
        displayDigits[tempDigits[0]] = tempSegmentLookup[0]; // Custom digit, " " char
        displayDigits[tempDigits[1]] = segmentLookup[l_E];
        displayDigits[tempDigits[2]] = segmentLookup[l_R];
        return;
    }
    else if (temp >= 100)
    {
        // Over 100
        displayDigits[tempDigits[0]] = tempSegmentLookup[1]; // Custom digit, 1 char

        // Force drawing 100
        if (drawSign)
        {
            displayDigits[tempDigits[0]] = tempSegmentLookup[2]; // Custom digit, +1 char
        }
    }

    // The remaining digits
    displayDigits[tempDigits[1]] = segmentLookup[Display::nth_digit(abs(temp), 1)];
    displayDigits[tempDigits[2]] = segmentLookup[Display::nth_digit(abs(temp), 0)];

    // Indoor/outdoor indicator
    if (indoor)
    {
        displayDigits[tempDigits[0]] |= s_DP;
    }
    if (outdoor)
    {
        displayDigits[tempDigits[0]] |= s_F;
    }
};

void Display::setTime(char *time, bool am, bool pm)
{
    // Don't draw over an error code.
    if (Display::hasElapsed(lastTimeAlt, ALT_DECAY))
    {
        return;
    }

    // First segment
    displayDigits[timeDigits[0]] = timeSegmentLookup[time[0] - '0'];

    // The remaining segments
    displayDigits[timeDigits[1]] = segmentLookup[time[1] - '0'];
    displayDigits[timeDigits[2]] = segmentLookup[time[2] - '0'];
    displayDigits[timeDigits[3]] = segmentLookup[time[3] - '0'];
    displayDigits[timeDigits[4]] = segmentLookup[time[4] - '0'];
    displayDigits[timeDigits[5]] = segmentLookup[time[5] - '0'];

    // AM/PM
    am ? bitSet(displayDigits[timeDigits[0]], 5) : bitClear(displayDigits[timeDigits[0]], 5);
    pm ? bitSet(displayDigits[timeDigits[0]], 7) : bitClear(displayDigits[timeDigits[0]], 7);
};

void Display::setErr(int err)
{
    // First segment
    displayDigits[timeDigits[0]] = timeSegmentLookup[0]; // Null

    // AM and PM lights
    displayDigits[timeDigits[0]] = bitSet(displayDigits[timeDigits[0]], 1);

    // The remaining segments
    displayDigits[timeDigits[1]] = segmentLookup[l_E]; // E
    displayDigits[timeDigits[2]] = segmentLookup[l_R]; // r
    displayDigits[timeDigits[3]] = segmentLookup[l_R]; // r
    displayDigits[timeDigits[4]] = segmentLookup[Display::nth_digit(err, 1)];
    displayDigits[timeDigits[5]] = segmentLookup[Display::nth_digit(err, 0)];

    // Save the last error
    lastTimeAlt = millis();
};

void Display::clearDisplay()
{
    // If we're displaying something alt
    if (Display::hasElapsed(lastTimeAlt, ALT_DECAY))
    {
        // Clear it
        lastTimeAlt = millis() - (ALT_DECAY * 1000);
    }
};

void Display::refresh(unsigned int scrTime)
{
    for (uint8_t segIdx = 0; segIdx < 16; segIdx++)
    {
        Display::writeRawDigit(displayDigits[segIdx], segIdx, displayDigits[segIdx]);
        delayMicroseconds(scrTime);
    }
}

uint8_t Display::iterate()
{
    // Advance to next index
    globSegIdx++;

    // Iterate through every row in the displayDigits arr
    globSegIdx = globSegIdx % (sizeof(displayDigits) / sizeof(displayDigits[0]));

    // Draw digit
    Display::writeRawDigit(displayDigits[allDigits[globSegIdx]], allDigits[globSegIdx]);

    // Send the current index back
    return allDigits[globSegIdx];
}

int Display::nth_digit(int val, int n)
{
    // A little messy
    return val / pows[n] % 10;
};

bool Display::hasElapsed(unsigned long time, int dur)
{
    bool result = time + (dur * 1000) > millis();

    return result;
};

int Display::cyclic(int i, int max)
{
    return (max + i) % max;
}

float Display::fastRound(float in, int precision)
{
    int res = round(in * pow(10, precision));
    return float(res) / pow(10, precision);
}
