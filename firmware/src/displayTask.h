/**
 * @brief Display refresh rtos task.
 *
 * @param params
 */

#include <Arduino.h>
#include <ArduinoLog.h>
#include "buttons.h"

#if !defined(DISPLAY)
#include <display.h>
#endif

static void displayThread(void *params)
{
    // Local Memory
    uint8_t idx = 0;
    TickType_t xLastFrame = 0;
    const TickType_t xFreq = 1;

    // Logging
    Log.info("Beginning Display thread on core %d!", xPortGetCoreID());

    // Get local instance of display
    Display *disp = static_cast<Display *>(params);

    // Configure buttons
    configureButtons();

    // Init
    xLastFrame = xTaskGetTickCount();

    for (;;)
    {
        idx = disp->iterate();

        vTaskDelayUntil(&xLastFrame, xFreq);

        updateButton(idx);
    }
}
