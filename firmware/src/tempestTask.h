/**
 * @file tempestTask.h
 * @brief Tempest Task for communicating with tempest API
 */

#if !defined(WEBSOCKETSCLIENT)
#include <WebSocketsClient.h>
#endif

#include <ArduinoJson.h>
#include <TimeLib.h>

// Wifi custom params
WiFiManagerParameter configParamDeviceId("your_device_id", "Device ID", "76989", 10);
WiFiManagerParameter configParamUserId("user_id", "User ID", "12345", 10);
WiFiManagerParameter configParamToken("access_token", "Access Token", "e12379e4-8434-4ef6-877e-ce893154882f", 72);

// TODO: move this somewhere else and clean this code!
enum Pressure
{
    baroRISING,
    baroFALLING,
    baroSTATIC
};

// ==== Memory ====
// Errors
static char error_key;
static uint8_t error_code;
// Wind
static float tempestWindSpeed = -1;
static float tempestWindDir;
// Pressure
static Pressure tempestBaroTrend;
static float tempestBaro = 10000;
// Temperature
static float tempestAirTemp = 999;
static float tempestFeelslikeTemp = tempestAirTemp;

WebSocketsClient webSocket;

// Message counter
int message_ct = 0;
int getMessageCt()
{
    return message_ct++;
}

// This is just to make reading the logs easier
char typeArr[11][16] = {
    "Error",
    "Disconnected",
    "Connected",
    "Text",
    "Bin",
    "Frag Text Start",
    "Frag Bin Start",
    "Fragment",
    "Fragment Fin",
    "Ping",
    "Pong",
};

void webSocketEvent(WStype_t type, uint8_t *payload, size_t length)
{
    if (type != WStype_PONG)
    {
        Log.infoln("Message received with message type %s payload %s.", typeArr[type], payload);
    }
    else
    {
        Log.verboseln("Received keepalive pong message.");
    }

    switch (type)
    {
    case WStype_DISCONNECTED:
        Log.infoln("[WSc] Disconnected!\n");

        error_key = 'C'; // connection failure
        error_code = 02; // websocket
        break;

    case WStype_CONNECTED:
        Log.infoln("[WSc] Connected to url: %s\n", payload);

        error_key = 'G'; // all good
        error_code = 02; // websocket connected
        break;

    case WStype_TEXT:
        Log.verboseln("[WSc] get text: %s\n", payload);

        // define json document with correct capacity
        const size_t capacity = JSON_ARRAY_SIZE(1) + JSON_ARRAY_SIZE(32) + JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(7) + JSON_OBJECT_SIZE(12) + JSON_OBJECT_SIZE(35) + 1110;
        DynamicJsonDocument doc(capacity);

        // const char* json = "{\"station_id\":23346,\"station_name\":\"Wendover Way\",\"public_name\":\"Wendover Way\",\"latitude\":42.95757,\"longitude\":-71.49607,\"timezone\":\"America/New_York\",\"elevation\":69.34413146972656,\"is_public\":true,\"status\":{\"status_code\":0,\"status_message\":\"SUCCESS\"},\"station_units\":{\"units_temp\":\"f\",\"units_wind\":\"mph\",\"units_precip\":\"in\",\"units_pressure\":\"inhg\",\"units_distance\":\"mi\",\"units_direction\":\"cardinal\",\"units_other\":\"imperial\"},\"outdoor_keys\":[\"timestamp\",\"air_temperature\",\"barometric_pressure\",\"station_pressure\",\"pressure_trend\",\"sea_level_pressure\",\"relative_humidity\",\"precip\",\"precip_accum_last_1hr\",\"precip_accum_local_day\",\"precip_accum_local_yesterday_final\",\"precip_minutes_local_day\",\"precip_minutes_local_yesterday_final\",\"wind_avg\",\"wind_direction\",\"wind_gust\",\"wind_lull\",\"solar_radiation\",\"uv\",\"brightness\",\"lightning_strike_last_epoch\",\"lightning_strike_last_distance\",\"lightning_strike_count\",\"lightning_strike_count_last_1hr\",\"lightning_strike_count_last_3hr\",\"feels_like\",\"heat_index\",\"wind_chill\",\"dew_point\",\"wet_bulb_temperature\",\"delta_t\",\"air_density\"],\"obs\":[{\"timestamp\":1601846842,\"air_temperature\":15,\"barometric_pressure\":1013.5,\"station_pressure\":1013.5,\"sea_level_pressure\":1022.3,\"relative_humidity\":64,\"precip\":0,\"precip_accum_last_1hr\":0,\"precip_accum_local_day\":0,\"precip_accum_local_yesterday\":0,\"precip_accum_local_yesterday_final\":0,\"precip_minutes_local_day\":0,\"precip_minutes_local_yesterday\":0,\"precip_minutes_local_yesterday_final\":0,\"precip_analysis_type_yesterday\":1,\"wind_avg\":0.8,\"wind_direction\":219,\"wind_gust\":1,\"wind_lull\":0.5,\"solar_radiation\":40,\"uv\":0.13,\"brightness\":4798,\"lightning_strike_last_epoch\":1599771905,\"lightning_strike_last_distance\":41,\"lightning_strike_count\":0,\"lightning_strike_count_last_1hr\":0,\"lightning_strike_count_last_3hr\":0,\"feels_like\":15,\"heat_index\":15,\"wind_chill\":15,\"dew_point\":8.2,\"wet_bulb_temperature\":11.3,\"delta_t\":3.7,\"air_density\":1.22528,\"pressure_trend\":\"steady\"}]}";

        DeserializationError err = deserializeJson(doc, payload);

        if (err)
        {
            Log.errorln("deserializeJson() failed with code %s", err.c_str());

            error_key = 'F'; // fault/data corruption
            error_code = 10; // JSON fault
        }
        else
        {
            // pull status code and status message to check completion
            int status_status_code = doc["status"]["status_code"];               // 0
            const char *status_status_message = doc["status"]["status_message"]; // "SUCCESS"

            // pull constant station information
            const char *serial_number = doc["serial_number"]; // "ST-00015541"
            const char *hub_sn = doc["hub_sn"];               // "HB-00016940"
            const char *data_type = doc["type"];              // "obs_st"
            const char *source = doc["source"];               // "mqtt"
            int device_id = doc["device_id"];                 // 23346
            int firmware_revision = doc["firmware_revision"]; // 134

            // pull summary object information
            JsonObject summary = doc["summary"];
            const char *summary_pressure_trend = summary["pressure_trend"];                                   // "rising"
            int summary_strike_count_1h = summary["strike_count_1h"];                                         // "0"
            int summary_strike_count_3h = summary["strike_count_3h"];                                         // "0"
            float summary_precip_total_1h = summary["precip_total_1h"];                                       // "0.0"
            float summary_strike_last_dist = summary["strike_last_dist"];                                     // "35"
            long summary_strike_last_epoch = summary["strike_last_epoch"];                                    // "1602107881"
            float summary_precip_accum_local_yesterday = summary["precip_accum_local_yesterday"];             // "0.0"
            float summary_precip_accum_local_yesterday_final = summary["precip_accum_local_yesterday_final"]; // "0.0"
            int summary_precip_analysis_type_yesterday = summary["precip_analysis_type_yesterday"];           // "1" 0 = none, 1 = Rain Check with user display on, 2 = Rain Check with user display off
            float summary_feels_like = summary["feels_like"];                                                 // "3.7"
            float summary_heat_index = summary["heat_index"];                                                 // "3.7"
            float summary_wind_chill = summary["wind_chill"];                                                 // "3.7"
            long summary_pulse_adj_ob_time = summary["pulse_adj_ob_time"];                                    // "1614291403"
            float summary_pulse_adj_ob_wind_avg = summary["pulse_adj_ob_wind_avg"];                           // "0.8"
            float summary_pulse_adj_ob_temp = summary["pulse_adj_ob_temp"];                                   // "3.8"

            // pull raining information from summary
            JsonArray summary_raining_minutes = doc["summary"]["raining_minutes"]; // "[0,0,0,0,0,0,0,0,0,0,0,0]"
            float raining_minutes_0 = summary_raining_minutes[0];
            float raining_minutes_1 = summary_raining_minutes[1];
            float raining_minutes_2 = summary_raining_minutes[2];
            float raining_minutes_3 = summary_raining_minutes[3];
            float raining_minutes_4 = summary_raining_minutes[4];
            float raining_minutes_5 = summary_raining_minutes[5];
            float raining_minutes_6 = summary_raining_minutes[6];
            float raining_minutes_7 = summary_raining_minutes[7];
            float raining_minutes_8 = summary_raining_minutes[8];
            float raining_minutes_9 = summary_raining_minutes[9];
            float raining_minutes_10 = summary_raining_minutes[10];
            float raining_minutes_11 = summary_raining_minutes[11];

            // doc = ("obs":[[1614216246,0.54,1.05,1.56,242,3,996.1,5.4,58,0,0.0,0,0.0,0,0,0,2.64,1,0.0,null,null,0]]);

            // pull observation information, formated as an array in an arrary
            // hence the obs_outer
            JsonArray obs_outer = doc["obs"];
            JsonArray obs = obs_outer[0];
            long obs_timestamp = obs[0];
            float obs_wind_lull = obs[1];    // 0.5
            float obs_wind_avg = obs[2];     // 0.8
            int obs_wind_gust = obs[3];      // 1
            int obs_wind_direction = obs[4]; // 219
            int obs_wind_sample_interval = obs[5];
            float obs_station_pressure = obs[6]; // 1013.5 in MB
            float obs_air_temperature = obs[7];  // 15 in C
            int obs_relative_humidity = obs[8];  // 64
            int obs_illuminance = obs[9];
            float obs_uv = obs[0, 10];         // 0.13
            int obs_solar_radiation = obs[11]; // 40
            int obs_rain_accum = obs[12];
            int obs_percipitation_type = obs[13]; // 0 = none, 1 = rain, 2 = hail
            int obs_lightning_strike_avg_distance = obs[14];
            int obs_lightning_strike_count = obs[15];                   // 0
            int obs_battery = obs[16];                                  // in volts
            long obs_report_interval = obs[17];                         // in minutes
            float obs_local_daily_rain_accum = obs[18];                 // in mm
            float obs_rain_accum_final_raincheck = obs[19];             // in mm
            float obs_local_daily_rain_accum_final_raincheck = obs[20]; // in mm
            int obs_precip_analysis_type = obs[21];                     // 0 = none, 1 = Rain Check with user display on, 2 = Rain Check with user display off

            // if connection established, start rapid wind...
            if (strcmp(data_type, "connection_opened") == 0)
            {
                Log.info("Sending rapid start");
                char _rapidStart[70];
                sprintf(_rapidStart, "{type:listen_rapid_start,device_id: %s,id:123456}", configParamDeviceId.getValue());
                webSocket.sendTXT(_rapidStart);
                // webSocket.sendTXT("{type:listen_rapid_start,device_id: " + custom_station_info_id.getValue() + ",id:"custom_station_info_user_id.getValue() + "}");

                error_key = 'G'; // all good
                error_code = 00; // all good
                delay(500);
            }
            // if rapid wind, start obs listening...
            else if (strcmp(data_type, "ack") == 0)
            {
                Log.infoln("Sending obs listening");
                data_type = "";
                char _obsStart[70];
                sprintf(_obsStart, "{type:listen_start,device_id:%s,id:2098388936}", configParamDeviceId.getValue());
                webSocket.sendTXT(_obsStart);

                error_key = 'G'; // all good
                error_code = 00; // all good
                delay(500);
            }
            // if receive wind info...
            else if (strcmp(data_type, "rapid_wind") == 0)
            {
                // parse wind info
                JsonArray wind_ob = doc["ob"];
                long wind_ob_time = wind_ob[0];
                float wind_ob_speed = wind_ob[1];
                float wind_ob_direction = wind_ob[2];

                // save relevant information globally for handshake
                tempestWindSpeed = wind_ob_speed;
                tempestWindDir = wind_ob_direction;

                // display wind info
                char buff_wind[32];
                sprintf(buff_wind, "%02d.%02d.%02d %02d:%02d:%02d", day(wind_ob_time), month(wind_ob_time), year(wind_ob_time), hour(wind_ob_time), minute(wind_ob_time), second(wind_ob_time));
                Serial.print("Wind UTC is ");
                Serial.println(buff_wind);
                Serial.print("Wind speed is ");
                Serial.println(wind_ob_speed);
                Serial.print("Wind direction is ");
                Serial.println(wind_ob_direction);
            }
            // if receive obs info...
            else if (strcmp(data_type, "obs_st") == 0)
            {
                // Decode pressure (move this somewhere else!)
                if (strcmp(summary_pressure_trend, "rising") == 0)
                {
                    tempestBaroTrend = baroRISING;
                }
                else if (strcmp(summary_pressure_trend, "falling") == 0)
                {
                    tempestBaroTrend = baroFALLING;
                }
                else
                {
                    tempestBaroTrend = baroSTATIC;
                }
                tempestBaro = obs_station_pressure;

                // Decode air temp
                tempestAirTemp = obs_air_temperature;
                tempestFeelslikeTemp = summary_wind_chill;

                // display station information for debugging
                // comment out if not using
                Serial.print("Serial number is ");
                Serial.println(serial_number);
                Serial.print("Hub serial number is ");
                Serial.println(hub_sn);
                Serial.print("Type is ");
                Serial.println(data_type);
                Serial.print("Source is ");
                Serial.println(source);
                Serial.print("Device ID is ");
                Serial.println(device_id);
                Serial.print("Firmware revision is ");
                Serial.println(firmware_revision);
                Serial.println();
                Serial.println("--Obs data--");

                // display weather information for debugging
                // comment out if not using
                char buff_st[32];
                sprintf(buff_st, "%02d.%02d.%02d %02d:%02d:%02d", day(obs_timestamp), month(obs_timestamp), year(obs_timestamp), hour(obs_timestamp), minute(obs_timestamp), second(obs_timestamp));
                Serial.print("UTC is ");
                Serial.println(buff_st);
                Serial.print("Time is ");
                Serial.println(obs_timestamp);
                Serial.print("Air temp is ");
                Serial.println((obs_air_temperature * 1.8) + 32);
                Serial.print("Relative humidity is ");
                Serial.println(obs_relative_humidity);
                Serial.print("Pressure trend is ");
                Serial.println(summary_pressure_trend);
                Serial.print("Station pressure is ");
                Serial.println(obs_station_pressure);
                Serial.print("Precip analysis type is ");
                Serial.println(obs_precip_analysis_type);
                Serial.print("Wind avg is ");
                Serial.println(obs_wind_avg);
                Serial.print("Wind direction is ");
                Serial.println(obs_wind_direction);
                Serial.print("Wind gust is ");
                Serial.println(obs_wind_gust);
                Serial.print("Wind lull is ");
                Serial.println(obs_wind_lull);
                Serial.print("Solar radiation is ");
                Serial.println(obs_solar_radiation);
                Serial.print("UV is ");
                Serial.println(obs_uv);
                Serial.print("Illuminance is ");
                Serial.println(obs_illuminance);
                Serial.print("Lightning strike average distance is ");
                Serial.println(obs_lightning_strike_avg_distance);
                Serial.print("Lightning strike count is ");
                Serial.println(obs_lightning_strike_count);
                Serial.print("Wind Sample Interval is ");
                Serial.println(obs_wind_sample_interval);
                Serial.print("Rain Accumulation is ");
                Serial.println(obs_rain_accum);
                Serial.print("Percipitation Type is ");
                Serial.println(obs_percipitation_type);
                Serial.print("Battery level is ");
                Serial.println(obs_battery);
                Serial.print("Report Interval is ");
                Serial.println(obs_report_interval);
                Serial.print("Local Daily Rain Accumulation is ");
                Serial.println(obs_local_daily_rain_accum);
                Serial.print("Rain Accumulation Final is ");
                Serial.println(obs_rain_accum_final_raincheck);
                Serial.print("Local Daily Rain Accumulation Final is ");
                Serial.println(obs_local_daily_rain_accum_final_raincheck);
            }
            else
            {
                Log.warning("%s didn't fit into any decodable category.", data_type);
            }
        }
    }
}

void tempestTask(void *pvParams)
{
    TickType_t last;
    myDelayMsUntil(&last, 5000); // Artificial startup delay
    Log.info("Beginning tempest thread on core %d!", xPortGetCoreID());

    // server address, port and URL
    char _wsHost[70];
    sprintf(_wsHost, "/swd/data?token=%s", configParamToken.getValue());
    Log.info("Connecting to api host %s", _wsHost);
    webSocket.begin("ws.weatherflow.com", 80, _wsHost);

    // event handler
    webSocket.onEvent(webSocketEvent);

    // try ever 5 secomds again if the connection has failed
    webSocket.setReconnectInterval(5000);

    // start heartbeat (optional)
    // ping server every 15000 ms
    // expect pong from server within 5000 ms
    // consider connection disconnected if pong is not received 2 times
    webSocket.enableHeartbeat(15000, 5000, 2);

    for (;;)
    {
        webSocket.loop();
        myDelayMs(100);
    }
}
