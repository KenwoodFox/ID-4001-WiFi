/**
 * @file rtos_delay.h
 * @author Joe
 * @brief Helper functions for rtos delays
 */

void myDelayUs(int us)
{
    vTaskDelay(us / configTICK_RATE_HZ);
}

void myDelayMs(int ms)
{
    vTaskDelay((ms * 1000) / configTICK_RATE_HZ);
}

void myDelayMsUntil(TickType_t *previousWakeTime, int ms)
{
    vTaskDelayUntil(previousWakeTime, (ms * 1000) / configTICK_RATE_HZ);
}