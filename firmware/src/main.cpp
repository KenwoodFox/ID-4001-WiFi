/**
 * @file main.cpp
 * @author Nick Soggu (nick@mail.nul)
 * @brief Source code for the ID-4001-WiFi project (https://gitlab.com/KenwoodFox/ID-4001-WiFi/)
 * @remarks Refactored by Joe Sedutto (joe.sedutto@silvertech.com)
 *
 * @copyright Copyright (c) 2022
 */

// Hardware
#include <SPI.h>
#include "Arduino.h"
#include <WiFiManager.h>

// FreeRTOS
#include "rtos_delay.h"

// Other libs
#include "ArduinoLog.h"
#include <NTPClient.h>
#include <TimeLib.h>

// Pins
#include "boardConfig.h"

// Custom libs
// #include "compass.h"
#include "display.h"
#include "conversions.h"

// Tasks
#include "displayTask.h"
#include "tempestTask.h"
TaskHandle_t Handle_displayTask;
TaskHandle_t Handle_CountUpTask;
TaskHandle_t Handle_DebugTask;
TaskHandle_t Handle_TempestTask;

// Objects
static Display display = Display(SS_A, SS_B, SS_C, SS_D, LATCH);

// Wifi
WiFiManager wifiManager;
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "time.google.com"); // Default to using google's server

// callback for failed connection to internet
void configModeCallback(WiFiManager *myWiFiManager)
{
    Log.warningln("We have entered wifi config mode. IP: %s SSID: %s", WiFi.softAPIP(), myWiFiManager->getConfigPortalSSID());
    display.setErr(1);
}

/**
 * @brief Get the extremes of a value
 *
 * @param arr     Array to iterate
 * @param arrLen  Length of array
 * @param extra   An extra value to compare with
 * @param max     Whether to return the max or min
 * @return float  The value returned
 */
float getExtreme(float arr[], byte arrLen, float extra, bool max = true)
{
    float ret = extra; // Preload with the first element
    for (uint8_t i = 0; i < arrLen; i++)
    {
        if (max)
        {
            ret = arr[i] > ret ? arr[i] : ret;
        }
        else
        {
            ret = arr[i] < ret ? arr[i] : ret;
        }
    }
    return ret;
}

// Tasks
static void interfaceThread(void *pvParameters)
{
    myDelayMs(250); // Artificial startup delay keeps this task from stepping on anyone elses toes during startup.
    Log.info("Beginning Counting thread on core %d!", xPortGetCoreID());

    // Configure Display Settings
    display.setFill(false);

    // Local Memory
    // Temp
    float airTempMax = -999;
    float airTempMin = 999;
    // Baro
    float baroSet = false; // Toggle so we only update once per 15m period
    float baroHistory[4] = {1000, 1000, 1000, 1000};
    byte baroHistoryLen = sizeof(baroHistory) / sizeof(baroHistory[0]);
    // Wind
    float windSpeedMax = -999;

    // FreeRTOS
    TickType_t xLastFrame = 0;

    for (;;)
    {
        // Set the wind heading
        // Mostly for demo purposes but this will come up again later
        display.setFill(windAvgBtn.isPressed() && tempestWindSpeed > 0); // Set fill when wind button pressed
        display.setFillWidth(tempestWindSpeed * 2);                      // Fillwidth (fix later)
        display.setHeading(tempestWindDir);                              // Set heading

        // Special mode for clear
        display.setClear(clearBtn.isPressed()); // Set clear/all on mode

        // Calculate the barometric pressure
        float _baro = tempestBaro;
        if (!baroRateBtn.isPressed()) // Check baro rate button
        {
            _baro = minBaroBtn.isPressed() ? getExtreme(baroHistory, baroHistoryLen, _baro, false) : _baro; // Min baro
            _baro = maxBaroBtn.isPressed() ? getExtreme(baroHistory, baroHistoryLen, _baro, true) : _baro;  // Max baro
            display.setBaro(baroUnitsSw.isPressed() ? round(_baro) : milibarToInchMG(_baro),                // Set the baro values
                            tempestBaroTrend == baroRISING,
                            tempestBaroTrend == baroFALLING);
        }
        else // Not pressing baro rate button
        {
            _baro = baroHistory[0] - _baro;
            display.setBaro(baroUnitsSw.isPressed() ? abs(round(_baro)) : abs(milibarToInchMG(_baro)), _baro > 0, _baro < 0, false); // No baro <= for no change
        }

        // Calculate the temperature
        bool _altTemp = false;                                                                                                                                                     // Used to auto flip indoor/outdoor
        float _temp = minTempBtn.isPressed() ? airTempMin : tempestAirTemp;                                                                                                        // Minimum air temp
        _temp = _altTemp ? temperatureRead() : _temp;                                                                                                                              // Swap in the alt temp if read
        _temp = windChillBtn.isPressed() ? (tempUnitsSw.isPressed() ? tempestFeelslikeTemp - tempestAirTemp : celsiusToFahrenheit(tempestFeelslikeTemp - tempestAirTemp)) : _temp; // Feels like/wind chill temp (this is so long because of unit conversion weirdness! ugh!)
        _temp = maxTempBtn.isPressed() ? airTempMax : _temp;                                                                                                                       // Max air temp
        _temp = minTempBtn.isPressed() ? airTempMin : _temp;                                                                                                                       // Min air temp
        display.setTemp(tempUnitsSw.isPressed() ? _temp : celsiusToFahrenheit(_temp),                                                                                              // Actual temperature
                        _altTemp,                                                                                                                                                  // If displaying indoor
                        !_altTemp,                                                                                                                                                 // If displaying outdoor
                        windChillBtn.isPressed());                                                                                                                                 // Whether to force draw signs or not

        // Calculate Wind speed
        float _windspeed = tempestWindSpeed;
        _windspeed = peakGustBtn.isPressed() ? windSpeedMax : _windspeed;
        display.setWindspeed(windSpeedKnotSw.isPressed() ? kmhToKnots(_windspeed) : windSpeedKmhSw.isPressed() ? _windspeed
                                                                                                               : kmhToMph(_windspeed));

        // Sync the ntp time why not (or at least try to)
        if (!timeClient.update())
        {
            // Log.warningln("Error updating timeclient.");
            // timeClient.forceUpdate();
        }

        // Update some of our own long-running stats (not populated by the API)
        if (tempestAirTemp < 999) // if readings valid
        {
            // Air temp
            airTempMin = tempestAirTemp < airTempMin ? tempestAirTemp : airTempMin; // Get min daily
            airTempMax = tempestAirTemp > airTempMax ? tempestAirTemp : airTempMax; // Get max daily

            // Wind Speed
            windSpeedMax = tempestWindSpeed > windSpeedMax ? tempestWindSpeed : windSpeedMax; // Get the max daily

            // Barometric pressure
            if (minute() % 15 == 0)
            {
                if (!baroSet)
                {
                    for (uint8_t i = 0; i < baroHistoryLen - 1; i++)
                    {
                        baroHistory[i] = baroHistory[i + 1]; // Scoot
                    }
                    baroHistory[baroHistoryLen - 1] = tempestBaro; // The -1 here is for array len being +1 more than array index
                    baroSet = true;
                }
            }
            else
            {
                baroSet = false;
            }
        }

        myDelayMsUntil(&xLastFrame, 100);
    }
}

static void debugInfo(void *pvParameters)
{
    myDelayMs(150); // Artificial startup delay
    Log.info("Beginning DebugInfo thread on core %d!", xPortGetCoreID());

    int prevWaterline = 1000000;

    char *time = (char *)malloc(8); // Make some space for time
    int adjustedHour;               // Hour compensating for local time
    bool _lockout = false;          // Used to lock/toggle the wifimanager reset

    for (;;)
    {
        int curWaterLine = xPortGetMinimumEverFreeHeapSize();
        if (curWaterLine < prevWaterline)
        {
            Log.verboseln("Waterline has dropped from %d to %d. (%d bytes)", prevWaterline, curWaterLine, prevWaterline - curWaterLine);
            prevWaterline = curWaterLine;
        }

        unsigned long t = timeClient.getEpochTime();                                      // Get the current epoch time
        int offset = -5;                                                                  // Todo, dynamically receive this timezone offset from esp configurator
        adjustedHour = (hour(t) + offset) > 0 ? hour(t) + offset : hour(t) + offset + 23; // Bound adjustedHour

        if (minTempBtn.isPressed() && maxTempBtn.isPressed()) // Special hotkey! Press both min and max to see the version
        {
            sprintf(time, "%02d%02d%02d", FWREVISION_YEAR, FWREVISION_MONTH, FWREVISION_VER);
        }
        else if (hrMthSw.isPressed()) // Special mode for manual config
        {
            sprintf(time,
                    "%02d%02d%02d",
                    0, 0, 6 - (hrMthSw.currentDuration() / 100));

            if (hrMthSw.currentDuration() > 6000 && !_lockout)
            {
                Log.warning("Would reconfigure wifi manager now. %d", hrMthSw.currentDuration());
                wifiManager.startConfigPortal("ID-4001-Config");
                _lockout = true;
            }
            else if (!hrMthSw.isPressed())
            {
                _lockout = false;
            }
        }
        else if (!timeClient.isTimeSet())
        {
            // Time has not been set!
            display.setErr(4);
        }
        else
        {
            sprintf(time,
                    "%02d%02d%02d",
                    railwayTimeSw.isPressed() ? (adjustedHour % 12) + 1 : adjustedHour + 1,
                    minute(t),
                    second(t));
        }

        display.setTime(time, adjustedHour + 1 <= 12, adjustedHour + 1 > 12);

        // Check buttons
        if (clearBtn.pressed())
        {
            Log.infoln("Cleared!");
        }

        myDelayMs(500);
    }
}

void setup()
{
    // Begin display
    display.begin();

    // // Begin serial
    Serial.begin(115200); // USB (debug)
    Serial.print("\n\n");
    Log.begin(LOG_LEVEL_VERBOSE, &Serial);
    Log.infoln("Beginning user code! Version %s", FWREVISION);

    // // Configure buttons
    // // configureButtons();

    // Create the rtos threads
    xTaskCreatePinnedToCore(displayThread,         // Function
                            "Display Thread",      // Name
                            10240,                 // Stack Size
                            (void *)&display,      // Args
                            tskIDLE_PRIORITY + 10, // Priority
                            &Handle_displayTask,   // Handler
                            1);                    // What core to run on (1 or 0)
    xTaskCreatePinnedToCore(interfaceThread,
                            "Count Thread",
                            10240,
                            NULL,
                            tskIDLE_PRIORITY + 2,
                            &Handle_CountUpTask,
                            0);
    xTaskCreatePinnedToCore(debugInfo,
                            "Debug Thread",
                            10240,
                            NULL,
                            tskIDLE_PRIORITY + 1,
                            &Handle_DebugTask,
                            0);
    xTaskCreatePinnedToCore(tempestTask,
                            "Tempest Thread",
                            10240,
                            NULL,
                            tskIDLE_PRIORITY + 1,
                            &Handle_TempestTask,
                            0);

    // WiFiManager configuration
    myDelayMs(200);
    wifiManager.setAPCallback(configModeCallback); // Return to this callback when in configuration mode.

    // Register Custom Parameters
    wifiManager.addParameter(&configParamDeviceId);
    wifiManager.addParameter(&configParamUserId);
    wifiManager.addParameter(&configParamToken);

    wifiManager.autoConnect("ID-4001-WiFi"); // Spawn a AD-HOC net

    // Configure ntp (might need to move this to wifimanager callback)
    timeClient.begin();
}

void loop()
{
    vTaskDelete(NULL); // https://www.esp32.com/viewtopic.php?t=12121
}

void loop1()
{
    vTaskDelete(NULL); // https://www.esp32.com/viewtopic.php?t=12121
}
