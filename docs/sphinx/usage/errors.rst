.. index::
    single: Errors

Error Codes
===========

Er 1 Wifi
---------

.. id4001ui::
    :time: _Err01

Visible when waiting for WiFi configuration.


Er 2 API
--------

.. id4001ui::
    :time: _Err02

Shows when the API server cannot be reached or other API decoding error.


Err 4 NTP
---------

.. id4001ui::
    :time: _Err04

Shows as long as time has not been set.
