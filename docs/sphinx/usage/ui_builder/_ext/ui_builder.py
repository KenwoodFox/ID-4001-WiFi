import os
import string
import random

from docutils.parsers.rst import directives

from sphinx.parsers import RSTParser
from docutils.frontend import OptionParser
from sphinx.util.docutils import SphinxDirective
from docutils.utils import new_document

from PIL import Image, ImageDraw

# === Constants ===
# Segment lookup table
segLookUp = [
    "ABCDEF",  # 0
    "BC",  # 1
    "ABDEG",  # 2
    "ABCDG",  # 3
    "BCFG",  # 4
    "ACDFG",  # 5
    "ACDEFG",  # 6
    "ABC",  # 7
    "ABCDEFG",  # 8
    "ABCDFG",  # 9
    "",  # nothing
    "ADEFG",  # E
    "EG",  # r
]


class ui_builder(SphinxDirective):
    has_content = True
    required_arguments = 0
    optional_arguments = 1
    final_argument_whitespace = True
    option_spec = {
        "switches": directives.unchanged,
        "angles": directives.unchanged,
        "lamps": directives.unchanged,
        "time": directives.unchanged,
        "temp": directives.unchanged,
        "baro": directives.unchanged,
        "wind": directives.unchanged,
    }

    def run(self):
        # Gen dir
        genpath = "_build/id4001ui"
        try:
            os.mkdir(genpath)
        except FileExistsError:
            pass

        # Get args
        swtiches = self.options.get("switches", "").split(",")
        lamps = self.options.get("lamps", "").replace(" ", "").split(",")
        angles = self.options.get("angles", "").replace(" ", "").split(",")
        time = [
            self.options.get("time", "")[i : i + 2]
            for i in range(0, len(self.options.get("time", "")), 2)
        ]
        temp = self.options.get("temp", "")
        baro = self.options.get("baro", "")
        wind = self.options.get("wind", "")

        # Background image
        im = Image.open("usage/ui_builder/resources/background.png").convert("RGBA")

        # Clock
        if len(time) >= 3:
            cursor = 220
            for div in time:
                for intr in div:
                    try:
                        _val = int(intr)
                    except ValueError:
                        if intr == "_":
                            _val = 10
                        if intr == "E":
                            _val = 11
                        if intr == "r":
                            _val = 12

                    seg = self.compositeSevenSegment((45, 65), _val)

                    im.paste(seg, (cursor, 135), seg)
                    cursor += 60
                cursor += 30

        # Temperature
        if "+" in temp:  # Special case! Temp h has a weird + symbol thingy..
            seg = self.compositePlusSegment((45, 65))
            im.paste(seg, (340, 310), seg)
            temp = temp.replace("+", "")  # Remove special symbol
        if len(temp) > 0:
            for i in range(2):
                seg = self.compositeSevenSegment((45, 65), int(temp[i]))

                im.paste(seg, ((60 * i) + 380, 310), seg)

        # Windspeed
        if len(wind) > 0:
            _dp = False
            if "." in wind:
                _dp = True
                wind = wind.replace(".", "")
            for i in range(2):
                seg = self.compositeSevenSegment(
                    (45, 65), int(wind[i]), _dp if i == 0 else False
                )

                im.paste(seg, ((60 * i) + 960, 200), seg)

        try:
            for angle in angles:
                ang = self.compositeAngleLamp()
                ang = ang.rotate(float(angle))
                im.paste(ang, (850, 75), ang)
        except ValueError:
            pass

        # Baro
        if len(baro) > 0:
            _dp = False
            if "." in baro:
                _dp = True
                baro = baro.replace(".", "")
            for i in range(4):
                seg = self.compositeSevenSegment(
                    (45, 65), int(baro[i]), _dp if i == 1 else False
                )

                im.paste(seg, ((60 * i) + 1470, 140), seg)

        # Buttons
        for i in range(9):
            btn = self.compositeButton((30, 50), True if str(i) in swtiches else False)
            im.paste(btn, ((180 * i) + 290, 600), btn)

        # Lamps
        lamp = self.compositeLamp()
        im.paste(lamp, (115, 140), lamp) if "am" in lamps else 0  # am
        im.paste(lamp, (115, 175), lamp) if "pm" in lamps else 0  # pm
        im.paste(lamp, (175, 310), lamp) if "indoor" in lamps else 0  # outdoor
        im.paste(lamp, (175, 345), lamp) if "outdoor" in lamps else 0  # indoor
        im.paste(lamp, (665, 310), lamp) if "fahrenheit" in lamps else 0  # fahrenheit
        im.paste(lamp, (665, 345), lamp) if "celsius" in lamps else 0  # celsius
        im.paste(lamp, (1230, 300), lamp) if "mph" in lamps else 0  # mph
        im.paste(lamp, (1230, 335), lamp) if "knots" in lamps else 0  # knots
        im.paste(lamp, (1230, 370), lamp) if "kmh" in lamps else 0  # kmh
        im.paste(lamp, (1300, 140), lamp) if "rising" in lamps else 0  # rising
        im.paste(lamp, (1300, 170), lamp) if "falling" in lamps else 0  # falling
        im.paste(lamp, (1850, 140), lamp) if "inches" in lamps else 0  # inches
        im.paste(lamp, (1850, 170), lamp) if "millibar" in lamps else 0  # millibar

        # Save and make static-able
        name = "".join(random.choices(string.ascii_uppercase + string.digits, k=9))
        im.save(f"{genpath}/{name}-id4001.png")

        return self.parse_rst(
            f"""
.. image:: ../{genpath}/{name}-id4001.png
    :width: 100%
    :alt: Auto Generated"""
        )

    # https://sammart.in/post/2021-05-10-external-data-sphinx-extension/
    def parse_rst(self, text):
        parser = RSTParser()
        parser.set_application(self.env.app)
        settings = OptionParser(
            defaults=self.env.settings,
            components=(RSTParser,),
            read_config_files=True,
        ).get_default_values()
        document = new_document("<rst-doc>", settings=settings)
        parser.parse(text, document)
        return document.children

    # Scales a simple shape [(x,y), (x,y)]
    def scale(self, shape, newScale):
        ret = []
        for xy in shape:
            ret.append((xy[0] * (newScale[0] / 100), xy[1] * (newScale[1] / 100)))
        return ret

    def compositeAngleLamp(self, radius=320, height=8):
        im = Image.new(mode="RGBA", size=(radius, radius))
        draw = ImageDraw.Draw(im)

        draw.rectangle(
            [(280, (im.size[1] / 2) - height), (im.size[0], (im.size[1] / 2) + height)],
            fill=(128, 0, 0, 255),
        )

        return im

    # Composites a dot
    def compositeLamp(self, _size=(20, 20)):
        im = Image.new(mode="RGBA", size=_size)
        draw = ImageDraw.Draw(im)

        # Bottom
        draw.ellipse(
            [(0, 0), (_size[0] - 1, _size[1] - 1)],
            fill=(128, 0, 0, 255),
        )

        return im

    # Composites a button
    def compositeButton(self, _size, pressed=False):
        im = Image.new(mode="RGBA", size=_size)
        draw = ImageDraw.Draw(im)
        topOffset = 0 if not pressed else _size[1] / 3

        # Bottom
        draw.ellipse(
            [
                (0, (_size[1] - 1) / 2),
                (_size[0] - 1, _size[1] - 1),
            ],
            fill="#4b5354",
        )

        # Middle
        draw.rectangle(
            [
                (0, ((_size[1] - 1) / 4) + topOffset),
                (_size[0] - 1, (_size[1] - 1) - (_size[1] - 1) / 4),
            ],
            fill="#4b5354",
        )

        # Top
        draw.ellipse(
            [
                (0, 0 + topOffset),
                (_size[0] - 1, ((_size[1] - 1) / 2) + topOffset),
            ],
            fill="#7b878a",
        )

        return im

    # Composites a normal seven segment
    def compositeSevenSegment(self, _size, value, dp=False):
        im = Image.new(mode="RGBA", size=_size)
        draw = ImageDraw.Draw(im)
        vLookup = segLookUp[value]

        _w = int(im.size[0] / 8)
        _skew = int(im.size[0] / 2)
        _skew2 = _skew / 2
        _fill = (128, 0, 0, 255)

        # A
        draw.line(
            self.scale([(5 + _skew, 5), (95, 5)], (im.size)),
            fill=_fill if "A" in vLookup else 0,
            width=_w,
        )
        # B
        draw.line(
            self.scale([(95, 10), (95 - _skew2, 45)], (im.size)),
            fill=_fill if "B" in vLookup else 0,
            width=_w,
        )
        # C
        draw.line(
            self.scale([(95 - _skew2, 55), (95 - _skew, 90)], (im.size)),
            fill=_fill if "C" in vLookup else 0,
            width=_w,
        )
        # D
        draw.line(
            self.scale([(5, 95), (95 - _skew, 95)], (im.size)),
            fill=_fill if "D" in vLookup else 0,
            width=_w,
        )
        # E
        draw.line(
            self.scale([(5 + _skew2, 55), (5, 90)], (im.size)),
            fill=_fill if "E" in vLookup else 0,
            width=_w,
        )
        # F
        draw.line(
            self.scale([(5 + _skew, 10), (5 + _skew2, 45)], (im.size)),
            fill=_fill if "F" in vLookup else 0,
            width=_w,
        )
        # G
        draw.line(
            self.scale([(5 + _skew2, 50), (95 - _skew2, 50)], (im.size)),
            fill=_fill if "G" in vLookup else 0,
            width=_w,
        )

        if dp:
            dpSizeX = _size[0] / 8
            dpSizeY = _size[1] / 8
            offset = (90, 90)
            draw.ellipse(
                self.scale(
                    [
                        (offset[0] - dpSizeX, offset[1] - dpSizeY),
                        (offset[0] + dpSizeX, offset[1] + dpSizeY),
                    ],
                    (im.size),
                ),
                fill=(128, 0, 0, 255),
            )

        return im

    # Composites a weird plus segment
    def compositePlusSegment(self, _size, plus=True):
        im = Image.new(mode="RGBA", size=_size)
        draw = ImageDraw.Draw(im)

        _w = int(im.size[0] / 8)
        _skew = int(im.size[0] / 2)
        _skew2 = _skew / 2
        _fill = (128, 0, 0, 255)

        # B
        draw.line(
            self.scale([(50, 10), (50 - _skew2, 40)], (im.size)),
            fill=_fill if plus else 0,
            width=_w,
        )
        # C
        draw.line(
            self.scale([(50 - _skew2, 60), (50 - _skew, 90)], (im.size)),
            fill=_fill if plus else 0,
            width=_w,
        )
        # G
        draw.line(
            self.scale([(5 + _skew2, 50), (95 - _skew2, 50)], (im.size)),
            fill=_fill,
            width=_w,
        )

        return im


def setup(app):
    app.add_directive("id4001ui", ui_builder)

    return {
        "version": "0.1",
        "parallel_read_safe": True,
        "parallel_write_safe": True,
    }
