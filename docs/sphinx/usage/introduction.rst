.. index::
    single: Introduction

Introduction
============

.. note::

    For the latest print docs, please find them here attached to the `Latest Release`_.

.. id4001ui::
    :time: 103005
    :temp: 71
    :baro: 29.83
    :wind: 2.1
    :switches: 8
    :angles: 67.5
    :lamps: am, indoor, fahrenheit, falling, mph, inches

Hello and welcome! Congrats on finding this project, we hope it serves you well!

The ID-4001 was made by heathkit as a weather computer center, where various outdoor sensors
could be tied into and displayed at ones home, unfortunatly over time, these outdoor sensors
have become damaged or lost.

The ID-4001-WiFi drop in replacement board aims to replace the motherboard with a new style-appropriate
replacement allowing for use of wifi instruments, local weather stations or other new IOT style weather sensors!

.. Links
.. _`Latest Release`: https://gitlab.com/KenwoodFox/ID-4001-WiFi/-/releases