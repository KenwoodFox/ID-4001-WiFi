KiCad
=====

.. index::
    single: KiCad
    single: EDA Software
    single: Git

The ID-4001-WiFi_ hardware is all open source and created using KiCad_, a free and open source
EDA software.

To get started, clone the ID-4001-WiFi_ repo down, and **initialize all submodules** witch contain the
project's library files.

.. tabs::
    .. group-tab:: Debian/Ubuntu
        .. code-block::

            # Install git
            sudo apt install git

            # Clone
            git clone https://gitlab.com/KenwoodFox/ID-4001-WiFi.git
            cd ID-4001-WiFi

            # Checkout submodules
            git submodule update --init --recursive

    .. code-tab:: shell Arch

        # Install git
        sudo pacman -S git

        # Clone
        git clone https://gitlab.com/KenwoodFox/ID-4001-WiFi.git
        cd ID-4001-WiFi

        # Checkout submodules
        git submodule update --init --recursive

    .. group-tab:: Mac/BSD
        .. code-block::
            
            # Install git
            pkg install git
            # Or just type "git" and the auto prompt should prompt for install

            # Clone
            git clone https://gitlab.com/KenwoodFox/ID-4001-WiFi.git
            cd ID-4001-WiFi

            # Checkout submodules
            git submodule update --init --recursive

    .. group-tab:: Windows/DOS

        Download `Git for Windows`_ here: 

        or use winget:
        
        .. code-block::

            winget install --id Git.Git -e --source winget

        Then clone and checkout git submodules

        .. code-block::
        
            # Clone
            git clone https://gitlab.com/KenwoodFox/ID-4001-WiFi.git
            cd ID-4001-WiFi

            # Checkout submodules
            git submodule update --init --recursive


Next install and launch kicad

.. tabs::
    .. group-tab:: Debian/Ubuntu

        Ubuntu repos are behind at time of writing,
        follow these instructions for an up to date PPA.

        https://www.kicad.org/download/ubuntu/

        Finally, launch the project;

        .. code-block::

            kicad hardware/ID-4001-WIFI/ID-4001-WIFI.kicad_pro

    .. code-tab:: shell Arch

        # Install KiCad
        sudo pacman -Syu kicad

        # Libraries and other bits, recommended for this and other projects.
        sudo pacman -Syu --asdeps kicad-library kicad-library-3d

        # Launch the project;
        kicad hardware/ID-4001-WIFI/ID-4001-WIFI.kicad_pro

    .. group-tab:: Mac/BSD

        See the instructions here

        https://www.kicad.org/download/macos/

        Finally, launch the project;
        
        .. code-block::

            kicad hardware/ID-4001-WIFI/ID-4001-WIFI.kicad_pro

    .. group-tab:: Windows/DOS

        See the instructions here

        https://www.kicad.org/download/windows/

        Enter the `hardware/ID-4001-WIFI` directory and launch
        the `.kicad_pro` file within.

.. Links
.. _`Git for Windows`: https://git-scm.com/download/win
.. _`id-4001-WiFi`: https://gitlab.com/KenwoodFox/ID-4001-WiFi/-/releases
.. _KiCAD: https://www.kicad.org/
