Welcome to ID-4001-WiFi's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   usage/introduction
   usage/controls
   usage/errors.rst
   assembly/software
   api/api
   contributing/kicad
