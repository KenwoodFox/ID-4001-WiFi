import os
import sys
import subprocess

# Revision/git information
release = (
    subprocess.check_output(
        [
            "git",
            "describe",
            "--abbrev=4",
            "--always",
            "--tags",
        ]
    )
    .strip()
    .decode("utf-8")
)

# Amend any custom extensions we have
sys.path.append(os.path.abspath("usage/ui_builder/_ext"))

# Doxygen/cpp docs
subprocess.call("make clean", shell=True)
subprocess.call("cd ../doxygen ; doxygen", shell=True)

breathe_projects = {"ID-4001-WiFi": "../doxygen/xml/"}
breathe_default_project = "ID-4001-WiFi"

# Project Information
project = "ID-4001-WiFi"
copyright = "2022-2022, To Be Announced"
author = "Nick Soggu, Joe Sedutto"

# General Config
extensions = [
    "sphinx.ext.autosectionlabel",
    "sphinx.ext.autodoc",
    "sphinx.ext.doctest",
    "sphinx.ext.mathjax",
    "sphinx.ext.viewcode",
    "sphinx.ext.imgmath",
    "sphinx.ext.todo",
    "sphinx_tabs.tabs",
    "ui_builder",
    "breathe",
]


# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]


# HTML output options
html_theme = "sphinx_rtd_theme"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
# html_static_path = ['_static']


# PDF output options
latex_elements = {"extraclassoptions": "openany,oneside"}

# latex_logo = "resources/TeamLogo.png"
