C++ API reference
#################

C++ Classes
===========

Compass
-------

The id-4001's compass uses 16 LEDs in a circle to display wind direction

.. doxygenclass:: Compass
    :project: ID-4001-WiFi
    :members:
    :private-members:


Display
-------

The id-4001's display is made from many 8 segment indicators, a few special indicators and some LEDs

.. doxygenclass:: Display
    :project: ID-4001-WiFi
    :members:
    :private-members:


C++ Other
=========

Error Codes
-----------

An error enumerator handles keeping track of all
the possible error codes.

.. doxygenenum:: Error
    :project: ID-4001-WiFi


Animations
----------

.. doxygenfile:: animations.cpp
    :project: ID-4001-WiFi


Buttons
-------

.. doxygenfile:: buttons.h
    :project: ID-4001-WiFi
    :sections: briefdescription func

conversions
-----------

.. doxygenfile:: conversions.h
    :project: ID-4001-WiFi
    :sections: briefdescription func


.. tempest
.. -------

.. .. doxygenfile:: tempestTask.h
..     :project: ID-4001-WiFi
..     :sections: briefdescription func
